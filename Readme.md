# Defect Detector
Program to automatically detect and classify defects of lumber boards.


## Requirements

- Python 3
- .NET Framework 4.6.1 environment


## Setup

- download and extract the data set **salumn1.zip** (spiltted in 3 archives) in the [Downloads](https://bitbucket.org/DavideGia/defectdetector/downloads/) section
- to generate the labelled defects folder:
    - install the required dependencies running *install-extract-data-dependencies*
    - move the python script **extractDataNoMemoryError.py** inside salumn1 folder
	- run **extractDataNoMemoryError.py**, at the end of the process the defects will be inside the *extracted_data* directory
- download and extract **no_defects.zip** at the following [link](https://bitbucket.org/DavideGia/defectdetector/downloads/no_defects.zip)


## Usage

In order to let the program to automatically load training set and/or test set, place in the same folder where the executable is running the following folders:

- train_data – for training samples
- test_data – for test samples
- raw_data – for all raw/unprocessed images
