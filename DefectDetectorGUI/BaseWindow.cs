﻿using DefectDetector.GUI.Utils;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace DefectDetector.GUI
{
    /// <summary>
    /// Base window class with utilities used by the other windows of the application.
    /// </summary>
    public class BaseWindow : Window
    {

        #region Fields
        /// <summary>
        /// The inputImages.
        /// </summary>
        protected List<FileInfo> inputImages;
        /// <summary>
        /// The task factory for UI.
        /// </summary>
        protected TaskFactory uiTaskFactory;
        /// <summary>
        /// The main task scheduler.
        /// </summary>
        protected TaskScheduler mainScheduler;
        #endregion

        #region Constructor
        public BaseWindow()
        {
            // Initialization
            this.inputImages = new List<FileInfo>();
            this.mainScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            this.uiTaskFactory = new TaskFactory(mainScheduler);
        }
        #endregion Constructor

        #region Labelled Defect Class Extractors
        /// <summary>
        /// The defect files produced by the python script are named as follows: XclassY_Z.bmp
        /// where X is the board name, Y the defect class and Z the defect count
        /// This method takes the Y value from the file name.
        /// </summary>
        /// <param name="filename">The defect name.</param>
        /// <returns>The defect class.</returns>
        protected int GetDefectClassFromFileName(string filename)
        {
            var s1 = Regex.Split(filename, "class");
            var s2 = Regex.Split(s1[1], "_");
            return Convert.ToInt32(s2[0]);
        }

        /// <summary>
        /// Method to extract the defect from the class inputImages. The input parameters will be filled with the defect images and their defect type.
        /// </summary>
        /// <returns></returns>
        protected async Task<Dictionary<Image<Gray, byte>, int>> ExtractDefectsFromTrainingImages()
        {
            return await Task.Run(() =>
            {
                var results = new Dictionary<Image<Gray, byte>, int>();

                // Processing the images class
                for (int i = 0; i < inputImages.Count(); i++)
                {
                    var filename = inputImages[i];
                    var image = new Image<Gray, byte>(new Bitmap(filename.FullName));
                    var defectClass = GetDefectClassFromFileName(filename.FullName);
                    results.Add(image, defectClass);
                }

                return results;
            });
        }

        /// <summary>
        /// Method to extract the defect from the class inputImages. The input parameters will be filled with the defect images paths and their defect type
        /// </summary>
        /// <returns></returns>
        protected async Task<Dictionary<int, Tuple<string, int>>> ExtractDefectsAndClassFromTrainingImages()
        {
            return await Task.Run(() =>
            {
                var results = new Dictionary<int, Tuple<string, int>>();
                // Processing the images class
                for (int i = 0; i < inputImages.Count(); i++)
                {
                    var filename = inputImages[i].FullName;
                    var defectClass = GetDefectClassFromFileName(filename);
                    var imageWithDefectClassTuple = new Tuple<string, int>(filename, defectClass);
                    results.Add(i, imageWithDefectClassTuple);
                }

                return results;
            });
        }
        #endregion Labelled Defect Class Extractors

        #region Image Display
        /// <summary>
        /// Assigns an EMGU.CV image to a windows image control        
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="imageControl">The windows image control where the image should be displayed.</param>
        /// <param name="image">The image to display</param>
        protected void SetToControl<TColor, TDepth>(System.Windows.Controls.Image imageControl, Image<TColor, TDepth> image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            SetToControl(imageControl, image.ToBitmap());
        }

        /// <summary>
        /// Assigns a bitmap to a windows image control.
        /// </summary>
        /// <param name="bitmap">The bitmap to display.</param>
        protected void SetToControl(System.Windows.Controls.Image imageControl, Bitmap bitmap)
        {
            imageControl.Source = ConvertToBitmapImage(bitmap);
        }

        /// <summary>
        /// Resets the zoom.
        /// </summary>
        /// <param name="zoomer">The zoomer.</param>
        protected void ResetZoom(ZoomBorder zoomer)
        {
            zoomer.Reset();
        }
        #endregion Image Display

        #region Converters
        /// <summary>
        /// Converts a Bitmap into a Bitmap Image since emgucv does not support wpf.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <returns>The bitmap image.</returns>
        protected BitmapImage ConvertToBitmapImage(Bitmap bitmap)
        {
            BitmapImage bitmapImage = new BitmapImage();

            using (var stream = new MemoryStream())
            {
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Seek(0, SeekOrigin.Begin);

                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
            }

            //DG: do not remove, otherwise the image will be disposed
            bitmapImage.Freeze();

            return bitmapImage;
        }

        /// <summary>
        /// Converts an Image into a Bitmap Image.
        /// </summary>
        /// <param name="image"></param>
        /// <returns>The image converted to Bitmap Image.</returns>
        protected BitmapImage ConvertToBitmapImage(Image image)
        {
            BitmapImage bitmapImage = new BitmapImage();

            using (var stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Seek(0, SeekOrigin.Begin);

                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
            }

            //DG: do not remove, otherwise the image will be disposed
            bitmapImage.Freeze();

            return bitmapImage;
        }

        /// <summary>
        /// Converts a Bitmap to Image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="bitmap">The Bitmap to convert.</param>
        /// <returns>The Bitmap converted to Image.</returns>
        protected Image<TColor, TDepth> BitmapToImage<TColor, TDepth>(Bitmap bitmap)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return new Image<TColor, TDepth>(bitmap);
        }
        #endregion Converters

        #region Save Image Utilities
        /// <summary>
        /// Creates the results directory if it does not exists.
        /// </summary>
        protected void CreateResultsDirectoryIfMissing()
        {
            CreateDirectoryIfMissing(Constants.ResultsDirectory);
        }

        /// <summary>
        /// Creates the specified directory if it does not exists.
        /// </summary>
        /// <param name="directory">The directory to create.</param>
        protected void CreateDirectoryIfMissing(string directory)
        {
            Directory.CreateDirectory(directory);
        }

        /// <summary>
        /// Saves a Emgu image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image to save.</param>
        /// <param name="name">The image name (with extension).</param>
        /// <param name="path">The path where the image should be saved to.</param>
        protected void SaveImage<TColor, TDepth>(Image<TColor, TDepth> image, string name, string path = Constants.ResultsDirectory)
            where TColor : struct, IColor
            where TDepth : new()
        {
            CreateDirectoryIfMissing(path);
            image.ToBitmap().Save(path + "/" + name);
        }
        #endregion Save Image Utilities
    }
}
