﻿namespace DefectDetector.Models
{
    /// <summary>
    /// The application working modes
    /// </summary>
    public enum WorkingModes : int
    {
        /// <summary>
        /// Default null mode
        /// </summary>
        NONE = -1,
        /// <summary>
        /// Classifies a single detect
        /// </summary>
        SINGLE_CLASSIFICATION = 0,
        /// <summary>
        /// Detects and classify defects from a lumber board
        /// </summary>
        DETECT_CLASSIFY = 1
    }
}
