﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace DefectDetector.GUI.Utils
{
    /// <summary>
    /// Image set utilities
    /// </summary>
    public class ImageSetUtils
    {
        private static readonly string[] Extensions = { ".bmp", ".png", ".jpg" };

        /// <summary>
        /// Given a directory, analyzes the files matching with the specified extensions and gets the defect count for each class
        /// </summary>
        /// <param name="directory">The directory to analyze</param>
        /// <param name="images">The matching files</param>
        /// <param name="defectsCountDictionary">Mapping [defect class -> count]</param>
        public static void GetDefectsCountForEachClass(string directory, out List<FileInfo> images, out Dictionary<int, long> defectsCountDictionary)
        {
            images = new List<FileInfo>();
            defectsCountDictionary = new Dictionary<int, long>();
            foreach (FileInfo file in FileUtils.GetFilesByExtensions(new DirectoryInfo(directory), Extensions))
            {
                images.Add(file);
                int defectClass = GetDefectClassFromFileName(file.Name);
                if (defectsCountDictionary.ContainsKey(defectClass))
                {
                    defectsCountDictionary[defectClass] += 1;
                }
                else
                {
                    defectsCountDictionary.Add(defectClass, 1);
                }
            }
        }

        /// <summary>
        /// Extracts the defect class from the file name. The file should be named as *classX_*, where * can be any value and X is the class number
        /// </summary>
        /// <param name="filename">The filename</param>
        /// <returns>returns the class value</returns>
        public static int GetDefectClassFromFileName(string filename)
        {
            var s1 = Regex.Split(filename, "class");
            var s2 = Regex.Split(s1[1], "_");
            return Convert.ToInt32(s2[0]);
        }
    }
}
