﻿using System.Collections.Generic;

namespace DefectDetector.GUI.Utils
{
    /// <summary>
    /// Utility class that maps each defect number to its corresponding description
    /// </summary>
    public class DefectsMapper
    {
        private const string notClassified = "Not classified in the doc";
        private static Dictionary<int, string> classNameMapping = new Dictionary<int, string>()
        {
            { 1, "Sound knot" },
            { 2, "Super sound knot" },
            { 3, "Dry knot" },
            { 4, "Loose knots" },
            { 5, "Black knot" },
            { 6, "Pin knot" },
            { 7, "Bark ringed knot" },
            { 8, "Decayed knot (or Rotten knot)" },
            { 9, notClassified },
            { 10, "Oval knot" },
            { 11, "Arris knot" },
            { 12, "Horn knot" },
            { 13, "Leaf knot (or Spike knot)" },
            { 14, "Edge knot" },
            { 15, "Scattered knots" },
            { 16, "Knot cluster" },
            { 17, "Straight drying shake" },
            { 18, "Oblique drying shake" },
            { 19, "Ring shake" },
            { 20, "Wane edge" },
            { 21, "Bark" },
            { 22, "Resin pocket" },
            { 23, "Bark pocket" },
            { 24, "Scar" },
            { 25, "Twisted fibers" },
            { 26, "Top rupture" },
            { 27, "Reaction wood" },
            { 28, "Burl" },
            { 29, "Resine streak" },
            { 30, "Log blue stain" },
            { 31, "Heavy rot" },
            { 32, "Annual ring width defect" },
            { 33, "Brown streak" },
            { 34, "Honey combing" },
            { 35, "Mould" },
            { 36, "Chip mark" },
            { 37, "Miscut" },
            { 38, "Torn grain" },
            { 39, "Insect holes" },
            { 40, "Insect damage" },
            { 41, "Heart" },
            { NO_DEFECT_CLASS, "No defect" }
        };

        /// <summary>
        /// This number represent the no-defect class
        /// </summary>
        public const int NO_DEFECT_CLASS = 500;

        /// <summary>
        /// Gets the defect description corresponding to de given defect number
        /// </summary>
        /// <param name="defectNumber">THe defect number</param>
        /// <returns>The defect description or "Not classified in the doc" if no mapping is found</returns>
        public static string GetDefectName(int defectNumber)
        {
            bool success = classNameMapping.TryGetValue(defectNumber, out var value);
            if (!success)
            {
                value = notClassified;
            }
            return value;
        }

    }
}
