﻿namespace DefectDetector.GUI.Utils
{
    public class Constants
    {
        public const string ResultsDirectory = "results";
        public const string TrainDataDirName = "train_data";
        public const string TestDataDirName = "test_data";
        public const string DataSetDirName = "raw_data";
    }
}
