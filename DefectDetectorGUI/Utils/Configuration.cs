﻿using DefectDetector.Models;

namespace DefectDetector.GUI.Utils
{
    /// <summary>
    /// The configuration
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// The degree of parallelism.
        /// </summary>
        public static ParallelismDegrees DegreeOfParallelism;
    }
}
