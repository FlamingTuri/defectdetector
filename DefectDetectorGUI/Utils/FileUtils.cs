﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DefectDetector.GUI.Utils
{
    /// <summary>
    /// File utilities
    /// </summary>
    public class FileUtils
    {
        /// <summary>
        /// Filter files from a directory by the input extensions.
        /// </summary>
        /// <param name="dir">The directory.</param>
        /// <param name="extensions">The extension to filter.</param>
        /// <returns>The files that match the input extensions.</returns>
        public static IEnumerable<FileInfo> GetFilesByExtensions(DirectoryInfo dir, params string[] extensions)
        {
            return dir.EnumerateFiles().Where(f => extensions.Contains(f.Extension));
        }
    }
}
