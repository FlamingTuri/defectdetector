﻿using DefectDetector.GUI.Utils;
using DefectDetector.Models;
using System.Windows;
using System.Windows.Controls;

namespace DefectDetector.GUI
{
    /// <summary>
    /// Logica di interazione per HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();

            StartButton.IsEnabled = false;
            AvailableModes.SelectedIndex = (int)WorkingModes.DETECT_CLASSIFY;
            ParallelismDegree.SelectedIndex = (int)ParallelismDegrees.Low;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            if (button.Tag != null)
            {
                Window window;
                var mode = (WorkingModes)button.Tag;
                switch (mode)
                {
                    case WorkingModes.SINGLE_CLASSIFICATION:
                        {
                            window = new SingleClassificationWindow();
                            break;
                        }
                    case WorkingModes.DETECT_CLASSIFY:
                        {
                            window = new DetectAndClassifyWindow();
                            break;
                        }
                    case WorkingModes.NONE:
                    default:
                        {
                            MessageBox.Show("You must first choose a working mode!", "Invalid selection");
                            return;
                        }
                }

                this.Hide();
                window.ShowDialog();
                this.Show();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combobox = sender as ComboBox;

            var selectedMode = (WorkingModes)combobox.SelectedIndex;

            StartButton.Tag = selectedMode;
            switch (selectedMode)
            {
                case WorkingModes.SINGLE_CLASSIFICATION:
                    {
                        ModeDescription.Text = "Classification of a single defect in a picture.";              
                        break;
                    }
                case WorkingModes.DETECT_CLASSIFY:
                    {
                        ModeDescription.Text = "Detection and classification of all defects in a picture.";
                        break;
                    }
                case WorkingModes.NONE:
                default:
                    {
                        ModeDescription.Text = "";
                        break;
                    }
            }

            StartButton.IsEnabled = selectedMode != WorkingModes.NONE;
        }

        private void ParallelismDegree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combobox = sender as ComboBox;
            var selected = combobox.SelectedIndex >= 0 ? (ParallelismDegrees)combobox.SelectedIndex : ParallelismDegrees.None;
            Configuration.DegreeOfParallelism = selected;
            switch (selected)
            {
                case ParallelismDegrees.Hybrid:
                    {
                        ParallelismModeDescription.Text = "Mixed execution, serial execution with concurrent sub-operations.";
                        break;
                    }
                case ParallelismDegrees.Low:
                    {
                        ParallelismModeDescription.Text = "Parallel execution.";
                        break;
                    }
                case ParallelismDegrees.Medium:
                    {
                        ParallelismModeDescription.Text = "Asynchronous task-oriented execution with limited number of tasks.";
                        break;
                    }
                case ParallelismDegrees.High:
                    {
                        ParallelismModeDescription.Text = "Asynchronous task-oriented execution with maximum throughput.";
                        break;
                    }
                default:
                case ParallelismDegrees.None:
                    {
                        ParallelismModeDescription.Text = "Serial execution.";
                        break;
                    }
            }
        }
    }
}
