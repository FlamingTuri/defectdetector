﻿using DefectDetector.GUI.Utils;
using DefectDetector.Models;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DefectDetector.GUI
{
    /// <summary>
    /// Logica di interazione per SingleClassificationWindow.xaml
    /// </summary>
    public partial class SingleClassificationWindow : BaseWindow
    {
        /// <summary>
        /// The test images.
        /// </summary>
        protected List<FileInfo> testImages;
        /// <summary>
        /// Flag to determine whether.
        /// </summary>
        private bool processing = false;

        public SingleClassificationWindow()
        {
            InitializeComponent();

            CreateResultsDirectoryIfMissing();

            selectTrainingSetFolderTextBox.Text = Path.Combine(Directory.GetCurrentDirectory(), Constants.ResultsDirectory);

            this.testImages = new List<FileInfo>();
            var trainFolder = Directory.Exists(Constants.TrainDataDirName) ? Path.Combine(Directory.GetCurrentDirectory(), Constants.TrainDataDirName) : "";
            if (!string.IsNullOrEmpty(trainFolder))
            {
                LoadTrainSet(trainFolder);
            }

            var testFolder = Directory.Exists(Constants.TestDataDirName) ? Path.Combine(Directory.GetCurrentDirectory(), Constants.TestDataDirName) : "";
            if (!string.IsNullOrEmpty(testFolder))
            {
                LoadTestSet(testFolder);
            }
        }

        #region Interface Handlers
        private void PurgeCacheButton_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(Constants.ResultsDirectory))
            {
                var files = Directory.GetFiles(Constants.ResultsDirectory, resultsNameTextBox.Text + "*");
                foreach (var file in files)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unable to purge cache!");
                    }
                }
            }
        }

        private void SelectTrainingSetFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                SelectedPath = Directory.GetCurrentDirectory()
            })
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadTrainSet(dialog.SelectedPath);
                }
            }
        }

        private void SelectTestSetFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                SelectedPath = Directory.GetCurrentDirectory()
            })
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadTestSet(dialog.SelectedPath);
                }
            }
        }

        private void SelectedTestImage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedIndex = testImageFolderListBox.SelectedIndex;
            if (selectedIndex != -1 && !processing)
            {
                var selectedFile = testImages[selectedIndex].FullName;

                if (!File.Exists(selectedFile))
                {
                    MessageBox.Show("Selected file doesn't exists!");
                    return;
                }

                SetToControl(selectedImageView, new Bitmap(selectedFile));
                classifyImageButton.IsEnabled = true;
            }
            else
            {
                classifyImageButton.IsEnabled = false;
            }
        }

        private async void ClassifyImageButton_Click(object sender, RoutedEventArgs e)
        {
            this.classifyImageButton.IsEnabled = false;
            this.purgeCacheButton.IsEnabled = false;

            int selectedIndex = testImageFolderListBox.SelectedIndex;
            if (selectedIndex >= 0 && selectedIndex < testImages.Count)
            {
                var selected = testImages[selectedIndex];
                if (!File.Exists(selected.FullName))
                {
                    MessageBox.Show("The selected file doesn't exists!");
                    return;
                }

                this.processing = true;

                this.progressBar.Value = 0;
                this.progressText.Text = "init...";

                // Retrieving test image           
                var selectedImage = new Image<Bgr, byte>(selected.FullName);

                // Retrieving the result name (if the filename is not specified, we fall back to the last folder name or 'training')
                var resultsFilename = resultsNameTextBox.Text;

                var detector = new WoodDefectDetector()
                {
                    DegreeOfParallelism = Configuration.DegreeOfParallelism
                };
                detector.TrainingProgress += Detector_TrainingProgress;
                detector.TrainingCompleted += Detector_TrainingCompleted;
                detector.TestProgress += Detector_TestProgress;
                detector.TestCompleted += Detector_TestCompleted;

                if (!string.IsNullOrEmpty(resultsFilename))
                {
                    detector.ResultTag = resultsFilename;
                }

                var imagesWithDefectClassPaths = await ExtractDefectsFromTrainingImages();

                //training
                await detector.TrainAsync(imagesWithDefectClassPaths.Keys.ToArray(), imagesWithDefectClassPaths.Values.ToArray());

                //testing
                var testResults = await detector.TestAsync(selectedImage.Convert<Gray, byte>());

                //show result
                ShowResult(testResults.First());
            }

            this.purgeCacheButton.IsEnabled = true;
            this.classifyImageButton.IsEnabled = true;

            this.processing = false;
        }
        #endregion

        #region Detector Event Handlers
        private void Detector_TrainingCompleted(object sender, DetectorCompletedEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value = progressBar.Maximum;
                progressText.Text = string.Format("Training completed! ({0:0.000}s)", TimeSpan.FromMilliseconds(e.TotalExecutionTime).TotalSeconds);
            });
        }

        private void Detector_TrainingProgress(object sender, DetectorProgressEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value += 1;
                progressText.Text = string.Format("Training: {0}/{1} ({2}ms)", progressBar.Value, progressBar.Maximum, e.ExecutionTime);
            });
        }

        private void Detector_TestCompleted(object sender, DetectorCompletedEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value = progressBar.Maximum;
                progressText.Text = string.Format("Classification completed! ({0:0.000}s)", TimeSpan.FromMilliseconds(e.TotalExecutionTime).TotalSeconds);
            });
        }

        private void Detector_TestProgress(object sender, DetectorProgressEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value += 1;
                progressText.Text = string.Format("Classifying: {0}/{1} ({2}ms)", progressBar.Value, progressBar.Maximum, e.ExecutionTime);
            });
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Shows the result of the classification.
        /// </summary>
        /// <param name="result">The result</param>
        private void ShowResult(TestResult result)
        {
            this.testImageFeatureSet.Items.Clear();
            this.predictionClassFeatureSet.Items.Clear();

            var validResult = result.IsInsideThresholdForVariance && result.IsInsideThresholdForSampleVariance;
            var defectName = DefectsMapper.GetDefectName(result.Prediction);

            if (result.Prediction == DefectsMapper.NO_DEFECT_CLASS)
            {
                // The test image was not a defect
                this.classifyImageResult.Text = string.Format("{0} - {1}", result.Prediction, defectName);
            }
            else
            {
                // The test image was a defect
                this.classifyImageResult.Text = string.Format("{0} - {1} ({2}, according to class distribution)", result.Prediction, defectName, validResult ? "correct" : "incorrect");
            }

            for (int i = 0; i < result.TestFeatureSet.Cols; i++)
            {
                this.testImageFeatureSet.Items.Add(result.TestFeatureSet[0, i].ToString("0.00000000"));
                this.predictionClassFeatureSet.Items.Add(result.ClassMeanFeatureSet[0, i].ToString("0.00000000"));
            }
        }

        /// <summary>
        /// Loads the training set images.
        /// </summary>
        /// <param name="path">The path</param>
        private void LoadTrainSet(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                selectTrainingSetFolderTextBox.Text = path;

                inputImages.Clear();

                // Adding files
                foreach (FileInfo file in FileUtils.GetFilesByExtensions(new DirectoryInfo(path), ".bmp", ".png", ".jpg"))
                {
                    inputImages.Add(file);
                }
            }
        }

        /// <summary>
        /// Loads the test set images.
        /// </summary>
        /// <param name="path">The path</param>
        private void LoadTestSet(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                selectTestSetFolderTextBox.Text = path;

                testImages.Clear();
                testImageFolderListBox.Items.Clear();

                foreach (FileInfo file in FileUtils.GetFilesByExtensions(new DirectoryInfo(path), ".bmp", ".png", ".jpg"))
                {
                    testImages.Add(file);
                    testImageFolderListBox.Items.Add(file);
                }
            }
        }
        #endregion
    }
}
