﻿using DefectDetector.GUI.Utils;
using DefectDetector.Models;
using DefectDetector.VRUtils;
using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Point = System.Drawing.Point;

namespace DefectDetector.GUI
{
    /// <summary>
    /// Interaction logic for DetectAndClassifyWindow.xaml
    /// </summary>
    public partial class DetectAndClassifyWindow : BaseWindow
    {
        /// <summary>
        /// The test images.
        /// </summary>
        protected List<FileInfo> testImages;
        /// <summary>
        /// Flag to determine whether.
        /// </summary>
        private bool processing = false;
        /// <summary>
        /// The collection that holds the current detected defectes
        /// </summary>
        private Dictionary<int, Tuple<Image<Bgr, byte>, int>> detectedDefects;

        /// <summary>
        /// Default constructor
        /// </summary>
        public DetectAndClassifyWindow() : base()
        {
            InitializeComponent();

            CreateResultsDirectoryIfMissing();

            selectTrainingSetFolderTextBox.Text = Path.Combine(Directory.GetCurrentDirectory(), Constants.ResultsDirectory);

            this.testImages = new List<FileInfo>();
            var trainFolder = Directory.Exists(Constants.TrainDataDirName) ? Path.Combine(Directory.GetCurrentDirectory(), Constants.TrainDataDirName) : "";
            if (!string.IsNullOrEmpty(trainFolder))
            {
                LoadTrainSet(trainFolder);
            }

            var testFolder = Directory.Exists(Constants.TestDataDirName) ? Path.Combine(Directory.GetCurrentDirectory(), Constants.TestDataDirName) : "";
            if (!string.IsNullOrEmpty(testFolder))
            {
                LoadTestSet(testFolder);
            }
        }

        #region Interface Handlers
        private void PurgeCacheButton_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(Constants.ResultsDirectory))
            {
                var files = Directory.GetFiles(Constants.ResultsDirectory, resultsNameTextBox.Text + "*");
                foreach (var file in files)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unable to purge cache!");
                    }
                }
            }
        }

        private void SelectTrainingSetFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                SelectedPath = Directory.GetCurrentDirectory()
            })
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadTrainSet(dialog.SelectedPath);
                }
                else
                {
                    selectTrainingSetFolderTextBox.Text = "";
                    return;
                }
            }
        }

        private void SelectTestImageFolderPathButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                SelectedPath = Directory.GetCurrentDirectory()
            })
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadTestSet(dialog.SelectedPath);
                }
                else
                {
                    selectTestImagePathTextBox.Text = "";
                    return;
                }
            }
        }

        private void SelectedTestImage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedIndex = testImageFolderListBox.SelectedIndex;
            if (selectedIndex != -1 && !processing)
            {
                var selectedFile = testImages[selectedIndex].FullName;

                if (!File.Exists(selectedFile))
                {
                    MessageBox.Show("Selected file doesn't exists!");
                    return;
                }

                SetToControl(Selected_Image, new Bitmap(selectedFile));
                classifyImageButton.IsEnabled = true;
            }
            else
            {
                classifyImageButton.IsEnabled = false;
            }
        }

        private void DetectedDefects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = detectedDefectsView.SelectedIndex;
            if (selectedIndex != -1)
            {
                var tuple = detectedDefects[selectedIndex];
                SetToControl(Selected_Defect_Image, tuple.Item1.Bitmap);
                defectNumberTextBlock.Text = tuple.Item2.ToString();
                defectNameTextBlock.Text = DefectsMapper.GetDefectName(tuple.Item2);
            }
        }

        private async void DetectAndClassifyImageDefectsButton_Click(object sender, RoutedEventArgs e)
        {
            this.classifyImageButton.IsEnabled = false;
            this.purgeTrainData.IsEnabled = false;

            int selectedIndex = testImageFolderListBox.SelectedIndex;
            if (selectedIndex >= 0 && selectedIndex < testImages.Count)
            {
                var selected = testImages[selectedIndex];
                if (!File.Exists(selected.FullName))
                {
                    MessageBox.Show("The selected file doesn't exists!");
                    return;
                }

                this.processing = true;

                this.progressBar.Value = 0;
                this.progressText.Text = "Initializing...";

                // Clear old detects
                this.detectedDefectsView.Items.Clear();
                detectedDefects?.Clear();
                
                // Retrieving test image           
                var testImage = new Image<Bgr, byte>(selected.FullName);

                // Retrieving the images and training
                var imagesWithDefectClass = await ExtractDefectsFromTrainingImages();

                // Retrieving the result name (if the filename is not specified, we fall back to the last folder name or 'training')
                var resultsFilename = resultsNameTextBox.Text;

                // Setting up the detector
                var detector = new WoodDefectDetector()
                {
                    DegreeOfParallelism = Configuration.DegreeOfParallelism
                };
                detector.TrainingProgress += Detector_TrainingProgress;
                detector.TrainingCompleted += Detector_TrainingCompleted;
                detector.TestCompleted += Detector_TestCompleted;

                if (!string.IsNullOrEmpty(resultsFilename))
                {
                    detector.ResultTag = resultsFilename;
                }

                this.progressText.Text = "Start training...";
                this.progressBar.Value = 0;
                this.progressBar.Maximum = imagesWithDefectClass.Count;

                // Training
                await detector.TrainAsync(imagesWithDefectClass.Keys.ToArray(), imagesWithDefectClass.Values.ToArray());

                // Some delay to let the user to read the total training time
                await Task.Delay(TimeSpan.FromSeconds(1));

                this.progressText.Text = "Start detection...";

                // Testing with blob detection
                var grayImg = testImage.Convert<Gray, byte>();
                // Retrieving R channel image
                ColorSpaceUtils.SplitChannels(testImage, out var Rchannel, out var Gchannel, out var Bchannel);
                // Filter image, retrieving the opposite and dilate for better rendition
                var blobsImage = Rchannel.ThresholdBinary(new Gray(160), new Gray(255)).Not().Dilate(5);

                var blobDetector = new CvBlobDetector();
                var blobs = new CvBlobs();
                blobDetector.Detect(blobsImage, blobs);
                Console.WriteLine("Blobs: " + blobs.Count());

                // Retrieving the blobs detected and filtering them by size
                var invalidBlobs = blobs.Where(b => !IsBlobValid(b.Value, testImage, 0.9));
                for (int i = 0; i < invalidBlobs.Count(); i++)
                {
                    blobs.Remove(invalidBlobs.ElementAt(i).Key);
                }

                detectedDefects = new Dictionary<int, Tuple<Image<Bgr, byte>, int>>(blobs.Count());
                var imageWithBlobs = blobDetector.DrawBlobs(blobsImage, blobs, CvBlobDetector.BlobRenderType.Color, 1.0);

                var defectAreaList = GetDefectsFromFile(selected);
                if (defectAreaList.Count > 0)
                {
                    var imageWithDrawnDefects = DrawDefectAreaOnImage(imageWithBlobs, defectAreaList.ToArray());
                    SetToControl(Selected_Image_Blob, imageWithDrawnDefects);
                }
                else
                {
                    SetToControl(Selected_Image_Blob, imageWithBlobs);
                }

                int count = 0;
                for (int i = 0; i < blobs.Values.Count; i++)
                {
                    var blob = blobs.Values.ElementAt(i);
                    var blobImage = testImage.GetSubRect(blob.BoundingBox);

                    var testResults = await detector.TestAsync(blobImage.Convert<Gray, byte>());
                    var testResult = testResults.First();

                    // Filter non defects
                    if (testResult.Prediction != DefectsMapper.NO_DEFECT_CLASS)
                    {
                        // Checking the result inside the class distribution
                        var validResult = testResult.IsInsideThresholdForVariance && testResult.IsInsideThresholdForSampleVariance;

                        detectedDefects.Add(count, Tuple.Create<Image<Bgr, byte>, int>(blobImage, testResult.Prediction));
                        detectedDefectsView.Items.Add(string.Format(validResult ? "Defect #{0} at ({1},{2})" : "Defect #{0} at ({1:C0},{2:C0}) (?)", count,
                            Convert.ToInt32(blob.Centroid.X), Convert.ToInt32(blob.Centroid.Y)));
                        count++;
                    }
                }

               this.progressText.Text = "Detection completed!";
            }

            this.purgeTrainData.IsEnabled = true;
            this.classifyImageButton.IsEnabled = true;

            this.processing = false;
        }

        #endregion

        #region Detector Event Handlers
        private void Detector_TrainingProgress(object sender, DetectorProgressEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value += 1;
                progressText.Text = string.Format("Training: {0}/{1} ({2}ms)", progressBar.Value, progressBar.Maximum, e.ExecutionTime);
            });
        }

        private void Detector_TrainingCompleted(object sender, DetectorCompletedEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value = progressBar.Maximum;
                progressText.Text = string.Format("Training completed! ({0}s)", TimeSpan.FromMilliseconds(e.TotalExecutionTime).TotalSeconds);
            });
        }

        private void Detector_TestCompleted(object sender, DetectorCompletedEventArgs e)
        {
            uiTaskFactory.StartNew(() =>
            {
                progressBar.Value = progressBar.Maximum;
                progressText.Text = string.Format("Classification completed! ({0}s)", TimeSpan.FromMilliseconds(e.TotalExecutionTime).TotalSeconds);
            });
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Checks if a blob area is below a certain threshold, computed according to the test image size.
        /// </summary>
        /// <param name="blob">The CvBlob.</param>
        /// <param name="testImage">The test image.</param>
        /// <param name="sizeThreshold">The percentage of the test image size the blob area must be below of.</param>
        /// <returns>true if the blob area is below the threshold.</returns>
        private bool IsBlobValid(CvBlob blob, Image<Bgr, byte> testImage, double sizeThreshold)
        {
            var x = blob.BoundingBox.X;
            var y = blob.BoundingBox.Y;
            var w = blob.BoundingBox.Width;
            var h = blob.BoundingBox.Height;

            if (blob.BoundingBox.IsEmpty || blob.Centroid.IsEmpty || blob.Area <= 0 ||
                w >= testImage.Width || w < 0 || h >= testImage.Height || h < 0 ||
                x >= testImage.Width || x < 0 || y >= testImage.Height || y < 0)
            {
                return false;
            }

            var blobImage = testImage.GetSubRect(blob.BoundingBox);
            var blobArea = blobImage.Width * blobImage.Height;
            var testImageArea = testImage.Width * testImage.Height;
            return (blobArea / testImageArea) < sizeThreshold;
        }

        /// <summary>
        /// Gets the list of defects from the def file.
        /// </summary>
        /// <param name="imageFileInfo">The image file information</param>
        /// <returns>The list of tuples that represents the top left corner and the bottom right corner of the rectangle that inscribe the defect</returns>
        private List<Tuple<Point, Point>> GetDefectsFromFile(FileInfo imageFileInfo)
        {
            var path = imageFileInfo.DirectoryName;
            var defectFileName = Path.GetFileNameWithoutExtension(imageFileInfo.FullName) + ".def";
            var defectFilePath = Path.Combine(path, defectFileName);

            var defectsAreaList = new List<Tuple<Point, Point>>();
            if (File.Exists(defectFilePath))
            {
                string[] lines = File.ReadAllLines(defectFilePath);

                // in each .def file defects starts from line 8
                for (int i = 7; i < lines.Length; i++)
                {
                    var values = Array.ConvertAll(lines[i].Split(' '), s => int.Parse(s));

                    var topLeftCorner = new Point(values[0], values[1]);
                    var bottomRightCorner = new Point(values[2], values[3]);

                    var t = new Tuple<Point, Point>(topLeftCorner, bottomRightCorner);
                    defectsAreaList.Add(t);
                }
            }

            return defectsAreaList;
        }

        /// <summary>
        /// Draws the defects areas on the input image.
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="defectsCornersList">The list of defect corners</param>
        /// <returns>The input image with all defects areas drawn on it</returns>
        private Image<Bgr, byte> DrawDefectAreaOnImage(Image<Bgr, byte> image, params Tuple<Point, Point>[] defectsCornersList)
        {
            var imageWithDrawnDefects = image.Clone();
            foreach (var def in defectsCornersList)
            {
                var topLeftCorner = def.Item1;
                var bottomRightCorner = def.Item2;
                var width = bottomRightCorner.X - topLeftCorner.X;
                var height = bottomRightCorner.Y - topLeftCorner.Y;
                var defectArea = new Rectangle(topLeftCorner.X, topLeftCorner.Y, width, height);
                imageWithDrawnDefects.Draw(defectArea, new Bgr(Color.Red), 2);
            }
            return imageWithDrawnDefects;
        }

        /// <summary>
        /// Loads the training set images.
        /// </summary>
        /// <param name="path">The path</param>
        private void LoadTrainSet(string path)
        {
            selectTrainingSetFolderTextBox.Text = path;
            inputImages.Clear();

            // Adding training files
            foreach (var file in FileUtils.GetFilesByExtensions(new DirectoryInfo(path), ".bmp", ".png", ".jpg"))
            {
                inputImages.Add(file);
            }
        }

        /// <summary>
        /// Loads the test set images.
        /// </summary>
        /// <param name="path">The path</param>
        private void LoadTestSet(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                selectTestImagePathTextBox.Text = path;

                testImages.Clear();
                testImageFolderListBox.Items.Clear();

                foreach (FileInfo file in FileUtils.GetFilesByExtensions(new DirectoryInfo(path), ".bmp", ".png", ".jpg"))
                {
                    testImages.Add(file);
                    testImageFolderListBox.Items.Add(file);
                }
            }
        }
        #endregion
    }
}
