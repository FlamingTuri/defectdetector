﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides commmon image filters.
    /// </summary>
    public class FilterUtils
    {
        /// <summary>
        /// Applies the mean filter to smooth the input image.
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <param name="size">The kernel size (square).</param>
        /// <returns>The smoothened image.</returns>
        public static Image<Gray, byte> MeanFilter(Image<Gray, byte> image, int size)
        {
            return MeanFilter(image, size, size);
        }

        /// <summary>
        /// Applies the mean filter to smooth the input image.
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <param name="kernelWidth">The width of the kernel.</param>
        /// <param name="kernelHeight">The height of the kernel.</param>
        /// <returns>The smoothened image.</returns>
        public static Image<Gray, byte> MeanFilter(Image<Gray, byte> image, int kernelWidth, int kernelHeight)
        {
            var width = image.Width;
            var height = image.Height;
            var meanFilteredImage = image.Clone();
            var edgeX = kernelWidth / 2;
            var edgeY = kernelHeight / 2;

            for (int y = edgeY; y < height - edgeY; y++)
            {
                for (int x = edgeX; x < width - edgeX; x++)
                {
                    int accumulator = 0;
                    for (int fy = 0; fy < kernelHeight; fy++)
                    {
                        for (int fx = 0; fx < kernelWidth; fx++)
                        {
                            accumulator += image.Data[y - edgeY + fy, x - edgeX + fx, 0];
                        }
                    }
                    meanFilteredImage.Data[y, x, 0] = (byte)(accumulator / (kernelWidth * kernelHeight));
                }
            }
            return meanFilteredImage;
        }

        /// <summary>
        /// Applies the median filter. Useful for noise removal.
        /// Note: uses the "not processing boundaries" approach.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The input image.</param>
        /// <param name="size">The kernel size (square).</param>
        /// <returns>The image after applying the filter.</returns>
        public static Image<TColor, TDepth> MedianFilter<TColor, TDepth>(Image<TColor, TDepth> image, int size)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return MedianFilter(image, size, size);
        }

        /// <summary>
        /// Applies the median filter. Useful for noise removal.
        /// Note: uses the "not processing boundaries" approach.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The input image.</param>
        /// <param name="kernelWidth">The width of the kernel.</param>
        /// <param name="kernelHeight">The height of the kernel.</param>
        /// <returns>The image after applying the filter.</returns>
        public static Image<TColor, TDepth> MedianFilter<TColor, TDepth>(Image<TColor, TDepth> image, int kernelWidth, int kernelHeight)
            where TColor : struct, IColor
            where TDepth : new()
        {
            if (kernelWidth == 1 || kernelHeight == 1)
            {
                throw new ArgumentException("kernel width and height should not be 1");
            }

            var width = image.Width;
            var height = image.Height;
            var medianFilteredImage = new Image<TColor, TDepth>(width, height);
            var edgeX = kernelWidth / 2;
            var edgeY = kernelHeight / 2;

            Parallel.For(edgeY, height - edgeY, y =>
            {
                for (int x = edgeX; x < width - edgeX; x++)
                {
                    List<TDepth> list = new List<TDepth>();
                    // do not parallelize
                    for (int fy = 0; fy < kernelHeight; fy++)
                    {
                        for (int fx = 0; fx < kernelWidth; fx++)
                        {
                            list.Add(image.Data[y - edgeY + fy, x - edgeX + fx, 0]);
                        }
                    }
                    list.Sort();
                    int median = (int)Math.Round((double)(list.Count / 2), MidpointRounding.AwayFromZero) - 1;
                    if (median < 0)
                    {
                        median = 0;
                    }
                    medianFilteredImage.Data[y, x, 0] = list[median];
                }
            });

            return medianFilteredImage;
        }
    }
}
