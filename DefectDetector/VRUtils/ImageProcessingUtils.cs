﻿using DefectDetector.Models;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides useful methods for image processing.
    /// </summary>
    public class ImageProcessingUtils
    {

        #region Low Quality Scaling
        /// <summary>
        /// Scales an image using Emgu CV
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="originalImage">The original image.</param>
        /// <param name="scaleFactor">The scale factor: 0.5 = image width is scaled down by half, 2 image width is doubled</param>
        /// <returns>The scaled image</returns>
        public static Image<TColor, TDepth> LowQualityScaling<TColor, TDepth>(Image<TColor, TDepth> originalImage, double scaleFactor)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return originalImage.Resize(scaleFactor, Inter.Cubic);
        }

        /// <summary>
        /// Given min, max scale and a scale increment step, returns an array of scaled images
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="img">The image.</param>
        /// <param name="minimumScaleFactor">The minimum scale factor.</param>
        /// <param name="maximumScaleFactor">The maximum scale factor.</param>
        /// <param name="scaleIncrementStep">The scale factor increment step.</param>
        /// <returns>The array of images, one for each scale factor within the specified range.</returns>
        public static Image<TColor, TDepth>[] LowQualityScaling<TColor, TDepth>(Image<TColor, TDepth> img, double minimumScaleFactor = 0.5, double maximumScaleFactor = 2.0, double scaleIncrementStep = 0.5)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var scaleFactors = new List<double>();
            for (var scale = minimumScaleFactor; scale <= maximumScaleFactor; scale += scaleIncrementStep)
            {
                scaleFactors.Add(scale);
            }
            return ScaleImageForEachScaleFactor(img, scaleFactors.ToArray());
        }

        /// <summary>
        /// Generates an array of images, one for each scale factor.
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="originalImage">The original image.</param>
        /// <param name="scaleFactors">The array of scale factors.</param>
        /// <returns>The array of images, one for each scale factor.</returns>
        private static Image<TColor, TDepth>[] ScaleImageForEachScaleFactor<TColor, TDepth>(Image<TColor, TDepth> originalImage, double[] scaleFactors)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var imageResizedArray = new Image<TColor, TDepth>[scaleFactors.Length];
            var partition = Partitioner.Create(0, scaleFactors.Length);
            Parallel.ForEach(partition, p =>
            {
                for (int i = p.Item1; i < p.Item2; i++)
                {
                    imageResizedArray[i] = LowQualityScaling(originalImage, scaleFactors[i]);
                }
            });
            return imageResizedArray;
        }
        #endregion Low Quality Scaling

        #region High Quality Scaling
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap HighQualityScalingImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var resizedImage = new Bitmap(width, height);

            // maintains DPI regardless of physical size
            resizedImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(resizedImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    // prevents ghosting around the image borders
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return resizedImage;
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image</returns>
        public static Bitmap HighQualityScaling<TColor, TDepth>(Image<TColor, TDepth> image, int width, int height)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return HighQualityScalingImage(image.ToBitmap(), width, height);
        }

        /// <summary>
        /// Gets the ratio between image width and height
        /// </summary>
        /// <param name="image">The image</param>
        /// <returns>The aspect ratio</returns>
        public static double GetAspectRatio(Image image)
        {
            var imageWidth = image.Width;
            var imageHeight = image.Height;
            return imageWidth / imageHeight;
        }

        /// <summary>
        /// Resizes an image to the given width preserving the width/height aspect ratio
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="width">The width</param>
        /// <returns>The resized image</returns>
        public static Bitmap HighQualityScalingByWidth(Image image, int width)
        {
            double aspectRatio = GetAspectRatio(image);
            int height = (int)(width / aspectRatio);
            return HighQualityScalingImage(image, width, height);
        }

        /// <summary>
        /// Resizes an image to the given width preserving the width/height aspect ratio
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image</param>
        /// <param name="width">The width</param>
        /// <returns>The resized image</returns>
        public static Bitmap HighQualityScalingByWidth<TColor, TDepth>(Image<TColor, TDepth> image, int width)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return HighQualityScalingByWidth(image.Bitmap, width);
        }

        /// <summary>
        /// Resizes an image to the given height preserving the width/height aspect ratio
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="height">The height</param>
        /// <returns>The resized image</returns>
        public static Bitmap HighQualityScalingByHeight(Image image, int height)
        {
            double aspectRatio = GetAspectRatio(image);
            int width = (int)(height * aspectRatio);
            return HighQualityScalingImage(image, width, height);
        }

        /// <summary>
        /// Resizes an image to the given height preserving the width/height aspect ratio
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image</param>
        /// <param name="heigth">The height</param>
        /// <returns>The resized image</returns>
        public static Bitmap HighQualityScalingByHeight<TColor, TDepth>(Image<TColor, TDepth> image, int heigth)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return HighQualityScalingByHeight(image.Bitmap, heigth);
        }
        #endregion High Quality Scaling

        #region Split In Sub Images
        /// <summary>
        /// Given a list of image paths, gets the minimum, maximum and average width and height.
        /// Each value is stored in a pair, the first element is the width, the second the hight
        /// </summary>
        /// <param name="imagesPathsList"></param>
        /// <param name="min">a pair storing the min width and height found</param>
        /// <param name="max">a pair storing the max width and height found</param>
        /// <param name="avg">a pair storing the average width and height</param>
        public static void MinMaxAvgWidthAndHeight(List<string> imagesPathsList, out Tuple<int, int> min, out Tuple<int, int> max, out Tuple<int, int> avg)
        {
            var minW = 0;
            var minH = 0;
            var maxW = 0;
            var maxH = 0;
            var avgW = 0;
            var avgH = 0;

            foreach (string imagePath in imagesPathsList)
            {
                using (var currentImage = new Bitmap(imagePath))
                {
                    var currentWidth = currentImage.Width;
                    var currentHeight = currentImage.Height;

                    if (currentWidth < minW)
                    {
                        minW = currentWidth;
                    }

                    if (currentHeight < minH)
                    {
                        minH = currentWidth;
                    }

                    if (currentWidth > maxW)
                    {
                        maxW = currentWidth;
                    }

                    if (currentHeight > maxH)
                    {
                        maxH = currentWidth;
                    }

                    avgW += currentWidth;
                    avgH += currentHeight;
                }
            }
            avgW /= imagesPathsList.Count;
            avgH /= imagesPathsList.Count;

            min = new Tuple<int, int>(minW, minH);
            max = new Tuple<int, int>(maxW, maxH);
            avg = new Tuple<int, int>(avgW, avgH);
        }

        /// <summary>
        /// Generates a list of square subimages from a grayscale image
        /// </summary>
        /// <param name="image">The input grayscale image.</param>
        /// <param name="subImageSize">The subimages size.</param>
        /// <param name="xStep">The absolute distance of one subimage from the next one. If less than subImageSize, the subimages will overlap.</param>
        /// <exception cref="ArgumentOutOfRangeException"> if subImageSize or xStep are bigger than the image width or height.</exception>
        /// <returns>A list of square subimages.</returns>
        public static List<Image<Gray, byte>> ComputeSquareSubImages(Image<Gray, byte> image, int subImageSize, int xStep)
        {
            int imageWidth = image.Width;
            int imageHeight = image.Height;
            if (subImageSize > imageWidth || subImageSize > imageHeight)
            {
                throw new ArgumentOutOfRangeException("subImageSize", "subImageSize param must be lower than the image width and height");
            }
            if (xStep > imageWidth || xStep > imageHeight)
            {
                throw new ArgumentOutOfRangeException("xStep", "xStep param must be lower than the image width and height");
            }
            var list = new List<Image<Gray, byte>>();
            for (int i = 0; i < imageWidth; i += xStep)
            {
                if (i + subImageSize <= imageWidth)
                {
                    list.Add(image.GetSubRect(new Rectangle(i, 0, subImageSize, subImageSize)));
                }
            }
            return list;
        }

        /// <summary>
        /// Generates a list of subimages with xStep width and yStep height
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The input image</param>
        /// <param name="xStep">The width of each subimage</param>
        /// <param name="yStep">The height of each subimage</param>
        /// <returns>The list of subimages</returns>
        public static List<Image<TColor, TDepth>> SplitInSubImages<TColor, TDepth>(Image<TColor, TDepth> image, int xStep, int yStep)
            where TColor : struct, IColor
            where TDepth : new()
        {
            int imageWidth = image.Width;
            int imageHeight = image.Height;

            if (xStep > imageWidth)
            {
                throw new ArgumentOutOfRangeException("xStep", "xStep param must be lower than the image width");
            }
            if (yStep > imageHeight)
            {
                throw new ArgumentOutOfRangeException("yStep", "yStep param must be lower than the image height");
            }

            var list = new List<Image<TColor, TDepth>>();
            for (int y = 0; y < imageHeight; y += yStep)
            {
                for (int x = 0; x < imageWidth; x += xStep)
                {
                    if (x + xStep <= imageWidth && y + yStep <= imageHeight)
                    {
                        list.Add(image.GetSubRect(new Rectangle(x, y, xStep, yStep)));
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Splits the given image in subimages with size imageWidth/widthPartitions * imageHeght/heightPartitions
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image to split</param>
        /// <param name="widthPartitions">Number of width partitions</param>
        /// <param name="heightPartitions">Number of height partitions</param>
        /// <returns>The list of subimages</returns>
        public static List<Image<TColor, TDepth>> SplitInSubImagesOfEqualSize<TColor, TDepth>(Image<TColor, TDepth> image, int widthPartitions, int heightPartitions)
            where TColor : struct, IColor
            where TDepth : new()
        {
            if (widthPartitions < 1)
            {
                throw new ArgumentException("widthPartitions value must be >= 1");
            }
            if (heightPartitions < 1)
            {
                throw new ArgumentException("heightPartitions value must be >= 1");
            }
            int width = image.Width;
            int heigth = image.Height;
            int xStep = width / widthPartitions;
            int yStep = heigth / heightPartitions;
            return SplitInSubImages(image, xStep, yStep);
        }

        /// <summary>
        /// Applies SplitInSubImagesOfEqualSize(Image<TColor, TDepth>, int, int) for each value specified in the widthRange and heightRange
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image to split</param>
        /// <param name="widthRange">The range of vaules of the widthPartitions. The Start value is inclusive, the End value is exlusive</param>
        /// <param name="heightRange">The range of vaules of the heightPartitions. The Start value is inclusive, the End value is exlusive</param>
        /// <returns>The list of subimages</returns>
        public static List<Image<TColor, TDepth>> SplitInSubImagesOfEqualSize<TColor, TDepth>(Image<TColor, TDepth> image, Range widthRange, Range heightRange)
            where TColor : struct, IColor
            where TDepth : new()
        {
            if (widthRange.Start >= widthRange.End)
            {
                throw new ArgumentException("widthRange.Start value should be < than widthRange.End value");
            }
            if (heightRange.Start >= heightRange.End)
            {
                throw new ArgumentException("heightRange.Start value should be < than heightRange.End value");
            }
            var result = new ConcurrentBag<Image<TColor, TDepth>>();
            Parallel.For(widthRange.Start, widthRange.End, widthPartitions =>
            {
                for (int heightPartitions = heightRange.Start; heightPartitions < heightRange.End; heightPartitions++)
                {
                    var partialResult = SplitInSubImagesOfEqualSize(image, widthPartitions, heightPartitions);
                    foreach (var i in partialResult)
                    {
                        result.Add(i);
                    }
                }
            });
            return new List<Image<TColor, TDepth>>(result.ToArray());
        }

        /// <summary>
        /// Gets the subimages from an image.
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image</param>
        /// <param name="widthSteps">The width window dimension steps (dividers).</param>
        /// <param name="heightSteps">The height window dimension steps (dividers).</param>
        /// <param name="shiftStep">Represents the window shift amount, computed as the current windows size / shiftStep (if skipShiftThreshold is not reached).</param>
        /// <param name="skipShiftThreshold">Represents the window dimension threshold below which the window is shifted by its entire dimension (in pixels).</param>
        /// <returns>The list of subimages paired with its center point relative to the original image</returns>
        public static List<Tuple<Image<TColor, TDepth>, Point>> GetSubImages<TColor, TDepth>(Image<TColor, TDepth> image, DimensionSteps widthSteps, DimensionSteps heightSteps, double shiftStep = 0.5, int skipShiftThreshold = 50)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var subImages = new List<Tuple<Image<TColor, TDepth>, Point>>();

            var width = image.Size.Width;
            var height = image.Size.Height;

            //check for out of bound arguments
            if (shiftStep <= 0)
            {
                throw new ArgumentException("The shift step must be positive!");
            }

            if (skipShiftThreshold < 0)
            {
                throw new ArgumentException("The skipShiftThreshold must be positive!");
            }

            if (widthSteps.Steps.Any(i => i >= width))
            {
                throw new ArgumentOutOfRangeException("One or more width steps are greater that the input image width!");
            }

            if (heightSteps.Steps.Any(i => i >= height))
            {
                throw new ArgumentOutOfRangeException("One or more height steps are greater that the input image height!");
            }

            // multiple sizes and shapes
            foreach (int currWidth in widthSteps.Steps)
            {
                foreach (int currHeight in heightSteps.Steps)
                {

                    int currWindowWidth = width / currWidth;
                    int currWindowHeight = height / currHeight;

                    int currXStep = currWindowWidth >= skipShiftThreshold ? (int)Math.Ceiling(currWindowWidth * shiftStep) : currWindowWidth;
                    int currYStep = currWindowHeight >= skipShiftThreshold ? (int)Math.Ceiling(currWindowHeight * shiftStep) : currWindowHeight;

                    int halfXStep = currXStep / 2;
                    int halfYStep = currYStep / 2;

                    // slide the window across the entire image repeatedly
                    for (int y = 0; y < height; y += currYStep)
                    {
                        for (int x = 0; x < width; x += currXStep)
                        {
                            if (x + currXStep <= width && y + currYStep <= height)
                            {
                                var sectionImg = image.GetSubRect(new Rectangle(x, y, currXStep, currYStep));
                                subImages.Add(Tuple.Create(sectionImg, new Point(x + halfXStep, y + halfYStep)));
                            }
                        }
                    }
                }
            }

            return subImages;
        }
        #endregion Split In Sub Images

        #region Contrast Operations
        /// <summary>
        /// Contrast stretching (often called normalization)
        /// </summary>
        /// <param name="image"></param>
        /// <returns>The normalized image</returns>
        public static Image<Gray, byte> ContrastStretching(Image<Gray, byte> image)
        {
            var width = image.Width;
            var height = image.Height;

            byte a = 0; // a
            byte b = byte.MaxValue; // b
            byte min = byte.MaxValue; // c
            byte max = 0; // d
            // can't parallel for this
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var current = image.Data[y, x, 0];
                    if (current < min)
                    {
                        min = current;
                    }
                    if (current > max)
                    {
                        max = current;
                    }
                }
            }
            var spaceRange = b - a; // b-a

            var stretchedImage = new Image<Gray, byte>(width, height);
            Parallel.For(0, height, y =>
            {
                for (int x = 0; x < width; x++)
                {
                    byte newValue = (byte)(((image.Data[y, x, 0] - min) * (spaceRange / (max - min))) + a);
                    stretchedImage.Data[y, x, 0] = newValue;
                }
            });
            return stretchedImage;
        }

        /// <summary>
        /// Contrast ehnancement using Emgu methods
        /// </summary>
        /// <typeparam name="TColor">The color type.</typeparam>
        /// <typeparam name="TDepth">Teh depth type.</typeparam>
        /// <param name="image">The image</param>
        /// <param name="gamma">The gamma value</param>
        /// <returns>The enhanced image</returns>
        public static Image<TColor, TDepth> ContrastEhnancement<TColor, TDepth>(Image<TColor, TDepth> image, double gamma = 1.8d)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var contrastEnhanced = image.Clone();
            contrastEnhanced._EqualizeHist();
            contrastEnhanced._GammaCorrect(gamma);
            return contrastEnhanced;
        }

        /// <summary>
        /// Apply contrast ehnancement using the input function.
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="contrastApplication">The function used to apply the enhancement</param>
        /// <returns>The contrast ehnanced image</returns>
        private static Image<Bgr, byte> ContrastEhnancement(Image<Bgr, byte> image, Func<int, int> contrastApplication)
        {
            var hsv = ColorSpaceUtils.ConvertToHSV(image);
            var ehnancedHsv = hsv.Clone();
            ColorSpaceUtils.SplitChannels(hsv, out var Hue, out var Saturation, out var Value);
            Parallel.For(0, Value.Height, y =>
            {
                for (int x = 0; x < Value.Width; x++)
                {
                    var currentData = Value.Data[y, x, 0];
                    var res = contrastApplication(currentData);
                    res = (res > 255) ? 255 : (res < 0) ? 0 : res;
                    ehnancedHsv.Data[y, x, 2] = (byte)res;
                }
            });
            return ehnancedHsv.Convert<Bgr, byte>();
        }

        /// <summary>
        /// Contrast ehnancement. Increments Value channel by a fixed value
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="valueIncrease">The increment</param>
        /// <returns>The contrast ehnanced image</returns>
        public static Image<Bgr, byte> ContrastEhnancement(Image<Bgr, byte> image, byte valueIncrease)
        {
            int f(int data) => data + valueIncrease;
            return ContrastEhnancement(image, f);
        }

        /// <summary>
        /// Contrast ehnancement. Increments Value channel by the chosen percentage
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="percentage">The pergentage of increment</param>
        /// <returns>The contrast ehnanced image</returns>
        public static Image<Bgr, byte> ContrastEhnancement(Image<Bgr, byte> image, int percentage)
        {
            int f(int data) => data + (data * percentage / 100);
            return ContrastEhnancement(image, f);
        }
        #endregion Contrast Operations
    }
}
