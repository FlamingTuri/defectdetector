﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides methods to compute color moments features.
    /// </summary>
    public class ColorMomentsDescriptors
    {
        /// <summary>
        /// Computes the image average value and standard deviation.
        /// The standard deviation V0, V1, V2 should be in the same order of TColor values. 
        /// Ex. Bgr -> V0 = Blue, V1 = Green, V2 = Red
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image.</param>
        /// <param name="average">The image average value.</param>
        /// <param name="standardDeviation">The image standard deviation.</param>
        public static void StandardDeviation<TColor, TDepth>(Image<TColor, TDepth> image, out TColor average, out MCvScalar standardDeviation)
            where TColor : struct, IColor
            where TDepth : new()
        {
            image.AvgSdv(out average, out standardDeviation);
        }

        /// <summary>
        /// Computes the image standard deviation.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="redDeviation">The red channel deviation value.</param>
        /// <param name="greenDeviation">The green channel deviation value.</param>
        /// <param name="blueDeviation">The blue channel deviation value.</param>
        public static void StandardDeviation(Image<Bgr, byte> image, out double redDeviation, out double greenDeviation, out double blueDeviation)
        {
            var width = image.Width;
            var height = image.Height;

            var N = width * height;
            // gets the average value of the image
            var mean = image.GetAverage();

            ColorSpaceUtils.SplitChannels(image, out var red, out var green, out var blue);

            redDeviation = 0;
            greenDeviation = 0;
            blueDeviation = 0;

            for (var j = 0; j < height; j++)
            {
                for (var i = 0; i < width; i++)
                {
                    redDeviation += Math.Pow(red.Data[j, i, 0] - mean.Red, 2);
                    greenDeviation += Math.Pow(green.Data[j, i, 0] - mean.Green, 2);
                    blueDeviation += Math.Pow(blue.Data[j, i, 0] - mean.Blue, 2);
                }
            }

            redDeviation = Math.Sqrt(redDeviation / N);
            greenDeviation = Math.Sqrt(greenDeviation / N);
            blueDeviation = Math.Sqrt(blueDeviation / N);
        }

        /// <summary>
        /// Measures the color distribution asymmetry. WARNING: the results could be NaN.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="redSkewness">The red channel skewness value.</param>
        /// <param name="greenSkewness">The green channel skewness value.</param>
        /// <param name="blueSkewness">The blue channel skewness value.</param>
        public static void Skewness(Image<Bgr, byte> image, out double redSkewness, out double greenSkewness, out double blueSkewness)
        {
            var width = image.Width;
            var height = image.Height;

            var N = width * height;

            // gets the average value of the image
            var mean = image.GetAverage();

            ColorSpaceUtils.SplitChannels(image, out var red, out var green, out var blue);

            redSkewness = 0;
            greenSkewness = 0;
            blueSkewness = 0;

            for (var j = 0; j < height; j++)
            {
                for (var i = 0; i < width; i++)
                {
                    redSkewness += Math.Pow(red.Data[j, i, 0] - mean.Red, 3);
                    greenSkewness += Math.Pow(green.Data[j, i, 0] - mean.Green, 3);
                    blueSkewness += Math.Pow(blue.Data[j, i, 0] - mean.Blue, 3);
                }
            }

            redSkewness = NthRoot(redSkewness / N, 3);
            greenSkewness = NthRoot(greenSkewness / N, 3);
            blueSkewness = NthRoot(blueSkewness / N, 3);
        }

        /// <summary>
        /// Computes the Nnth Root of the specified value.
        /// </summary>
        /// <param name="radicand">The radicand value.</param>
        /// <param name="index">The index of the root.</param>
        /// <returns></returns>
        private static double NthRoot(double radicand, int index)
        {
            return Math.Pow(radicand, (1.0 / index));
        }
    }
}
