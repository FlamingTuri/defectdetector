﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which allows to detect lines, triangles, rectangles and circles inside the image.
    /// </summary>
    public class ShapeDetector
    {
        /// <summary>
        /// The Canny detector threshold.
        /// </summary>
        private const double CannyThreshold = 180.0;

        /// <summary>
        /// Wrapper to create a new Bgr image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image to convert.</param>
        /// <returns>A new Bgr image.</returns>
        private static Image<Bgr, byte> CreateNewBgrImage<TColor, TDepth>(Image<TColor, TDepth> image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            return new Image<Bgr, byte>(image.Width, image.Height);
        }

        /// <summary>
        /// Converts an image to UMat.
        /// </summary>
        /// <param name="image">The image to convert.</param>
        /// <returns>The UMat.</returns>
        private static UMat ConvertToGrayUmat(Image<Bgr, byte> image)
        {
            // Convert the image to grayscale
            UMat uimage = new UMat();
            CvInvoke.CvtColor(image, uimage, ColorConversion.Bgr2Gray);
            return uimage;
        }

        /// <summary>
        /// Removes image's noise using Pyr approach.
        /// The trasformation is in-place.
        /// </summary>
        /// <param name="uimage">The image.</param>
        private static void PyrNoiseRemoval(UMat uimage)
        {
            // Noise removal with image pyr
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);
        }

        /// <summary>
        /// Detects the image circles.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="circles">The detected circles.</param>
        public static void DetectCircles(Image<Bgr, byte> image, out CircleF[] circles)
        {
            var uimage = ConvertToGrayUmat(image);
            DetectCircles(uimage, out circles);
        }

        /// <summary>
        /// Detects the image circles.
        /// </summary>
        /// <param name="uimage">The image.</param>
        /// <param name="circles">The detected circles.</param>
        /// <param name="cannyThreshold">The Canny threshold applied.</param>
        public static void DetectCircles(UMat uimage, out CircleF[] circles, double cannyThreshold = CannyThreshold)
        {
            PyrNoiseRemoval(uimage);
            double circleAccumulatorThreshold = 120;
            circles = CvInvoke.HoughCircles(uimage, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);
        }

        /// <summary>
        /// Computes edges using canny edge detector.
        /// </summary>
        /// <param name="uimage">The image.</param>
        /// <param name="cannyEdges">The result of the detection.</param>
        public static void EdgeDetection(UMat uimage, out UMat cannyEdges, double cannyThreshold = CannyThreshold, double cannyThresholdLinking = 120.0)
        {
            cannyEdges = new UMat();
            CvInvoke.Canny(uimage, cannyEdges, cannyThreshold, cannyThresholdLinking);
        }

        /// <summary>
        /// Detects the image lines.
        /// </summary>
        /// <param name="uimage">The image.</param>
        /// <param name="lines">The detected lines.</param>
        public static void LinesDetection(UMat uimage, out LineSegment2D[] lines)
        {
            EdgeDetection(uimage, out var cannyEdges);
            lines = CvInvoke.HoughLinesP(
               cannyEdges,
               1, //Distance resolution in pixel-related units
               Math.PI / 45.0, //Angle resolution measured in radians.
               1); //threshold
                   //30, //min Line width
                   //10); //gap between lines
        }

        /// <summary>
        /// Detects the image triangles and rectangles.
        /// </summary>
        /// <param name="uimage">The image</param>
        /// <param name="lines">The lines detected.</param>
        /// <param name="triangleList">The triangles detected.</param>
        /// <param name="rectangleList">The rectangles detected.</param>
        public static void TriangleAndRectangleDetection(UMat uimage, out LineSegment2D[] lines, out List<Triangle2DF> triangleList, out List<RotatedRect> rectangleList)
        {
            EdgeDetection(uimage, out var cannyEdges);
            LinesDetection(uimage, out lines);

            #region Find triangles and rectangles
            triangleList = new List<Triangle2DF>();
            rectangleList = new List<RotatedRect>(); //a box is a rotated rectangle

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                int count = contours.Size;
                for (int i = 0; i < count; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                        if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                        {
                            Point[] pts = approxContour.ToArray();
                            triangleList.Add(new Triangle2DF(
                               pts[0],
                               pts[1],
                               pts[2]
                               ));
                        }
                        else if (approxContour.Size == 4) //The contour has 4 vertices.
                        {
                            #region determine if all the angles in the contour are within [80, 100] degree
                            bool isRectangle = true;
                            Point[] pts = approxContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);
                            #endregion

                            if (isRectangle)
                            {
                                rectangleList.Add(CvInvoke.MinAreaRect(approxContour));
                            }
                        }
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// Draws the circles on the image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image.</param>
        /// <param name="circles">The circles to draw.</param>
        /// <returns>The image with the drawn circles.</returns>
        public static Image<Bgr, byte> DrawCircles<TColor, TDepth>(Image<TColor, TDepth> image, List<CircleF> circles)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var circleImage = CreateNewBgrImage(image);

            foreach (CircleF circle in circles)
            {
                circleImage.Draw(circle, new Bgr(Color.Red), 2);
            }
            return circleImage;
        }

        /// <summary>
        /// Draws the lines on the image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image.</param>
        /// <param name="lines">The lines to draw.</param>
        /// <returns>The image with the drawn lines.</returns>
        public static Image<Bgr, byte> DrawLines<TColor, TDepth>(Image<TColor, TDepth> image, LineSegment2D[] lines)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var lineImage = CreateNewBgrImage(image);
            foreach (LineSegment2D line in lines)
            {
                lineImage.Draw(line, new Bgr(Color.Green), 2);
            }
            return lineImage;
        }

        /// <summary>
        /// Draws the triangles and the rectangles on the image.
        /// </summary>
        /// <typeparam name="TColor"></typeparam>
        /// <typeparam name="TDepth"></typeparam>
        /// <param name="image">The image.</param>
        /// <param name="triangleList">The triangles to draw.</param>
        /// <param name="rectangleList">The rectangles to draw.</param>
        /// <returns>The image with the drawn triangles and rectangles.</returns>
        public static Image<Bgr, byte> DrawTrianglesAndRectangles<TColor, TDepth>(Image<TColor, TDepth> image, List<Triangle2DF> triangleList, List<RotatedRect> rectangleList)
            where TColor : struct, IColor
            where TDepth : new()
        {
            Image<Bgr, byte> triangleRectangleImage = CreateNewBgrImage(image);
            foreach (Triangle2DF triangle in triangleList)
            {
                triangleRectangleImage.Draw(triangle, new Bgr(Color.DarkBlue), 2);
            }
            foreach (RotatedRect rectangle in rectangleList)
            {
                triangleRectangleImage.Draw(rectangle, new Bgr(Color.DarkOrange), 2);
            }
            return triangleRectangleImage;
        }
    }
}
