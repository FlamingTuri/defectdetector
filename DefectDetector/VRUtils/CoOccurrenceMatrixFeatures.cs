﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;

namespace DefectDetector.Utils
{
    /// <summary>
    /// Static class which provides co-occurrence matrix based features.
    /// </summary>
    public class CoOccurrenceMatrixFeatures
    {

        #region CoOccurrenceMatrix
        /// <summary>
        /// Computes the unoriented co-occurrence matrix (average of the four oriented matrices).
        /// </summary>
        /// <param name="img">The input grayscale image.</param>
        /// <returns>The unoriented co-occurence matrix.</returns>
        public static Matrix<double> ComputeUnorientedCoOccurrenceMatrix(Image<Gray, byte> img)
        {
            var m1 = ComputeCoOccurrenceMatrix(img, -1, 0);
            var m2 = ComputeCoOccurrenceMatrix(img, -1, -1);
            var m3 = ComputeCoOccurrenceMatrix(img, 0, -1);
            var m4 = ComputeCoOccurrenceMatrix(img, +1, -1);
            var res = m1.Add(m2).Add(m3).Add(m4);
            return res / 4;
        }

        /// <summary>
        /// Computes the co-occurrence matrix.
        /// </summary>
        /// <param name="img">The img.</param>
        /// <param name="dx">The x direction.</param>
        /// <param name="dy">The y direction.</param>
        /// <returns>The co-occurrence matrix.</returns>
        public static Matrix<double> ComputeCoOccurrenceMatrix(Image<Gray, byte> img, int dx, int dy)
        {
            int height = img.Height;
            int width = img.Width;

            int grayLevels = 256;
            Matrix<double> res = new Matrix<double>(grayLevels, grayLevels);
            int count = 0;
            for (int y = 0; y < height; y++)
            {
                var yy = y + dy;
                if (yy >= 0 && yy < height)
                {
                    for (int x = 0; x < width; x++)
                    {
                        var xx = x + dx;
                        if (xx >= 0 && xx < width)
                        {
                            count++;
                            res[img.Data[y, x, 0], img.Data[yy, xx, 0]] += 1.0;
                        }
                    }
                }
            }
            return res / count;
        }

        /// <summary>
        /// Computes the ith entry in the marginal-probability matrix.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="i">The ith entry value.</param>
        /// <returns>The computation result.</returns>
        public static double Px(Matrix<double> matrix, int i)
        {
            double ithEntry = 0.0;
            for (int j = 1; j < matrix.Cols; j++)
            {
                ithEntry += matrix[i, j];
            }
            return ithEntry;
        }

        /// <summary>
        /// Computes the jth entry in the marginal-probability matrix
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="j">The jth entry value.</param>
        /// <returns>The computation result.</returns>
        public static double Py(Matrix<double> matrix, int j)
        {
            double jthEntry = 0.0;
            for (int i = 1; i < matrix.Rows; i++)
            {
                jthEntry += matrix[i, j];
            }
            return jthEntry;
        }

        /// <summary>
        /// Adds the value in position [i, j] only if i + j == k
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="k">The value that should be equal to i + j.</param>
        /// <returns>The computation result.</returns>
        public static double Pxplusy(Matrix<double> matrix, int k)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double result = 0.0;
            for (int i = 1; i < rows; i++)
            {
                for (int j = 1; j < cols; j++)
                {
                    if (i + j == k)
                    {
                        result += matrix[i, j];
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Adds the value in position [i, j] only if |i - j| == k
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="k">The value that should be equal to |i - j|.</param>
        /// <returns>The computation result.</returns>
        public static double Pxminusy(Matrix<double> matrix, int k)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double result = 0.0;
            for (int i = 1; i < rows; i++)
            {
                for (int j = 1; j < cols; j++)
                {
                    if (Math.Abs(i - j) == k)
                    {
                        result += matrix[i, j];
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Mean of px and py.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="µx">Mean of px</param>
        /// <param name="µy">Mean of py</param>
        /// <returns>(px + py) / 2</returns>
        public static double Mean(Matrix<double> matrix, out double µx, out double µy)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            µx = 0.0;
            µy = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    µx += r * matrix[r, c];
                    µy += c * matrix[r, c];
                }
            }
            return (µx + µy) / 2;
        }

        /// <summary>
        /// Computes the standard deviation.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="ox">x standard deviation.</param>
        /// <param name="oy">y standard deviation.</param>
        public static void ComputeStandardDeviation(Matrix<double> matrix, out double ox, out double oy)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            ox = 0.0;
            oy = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var curr = matrix[r, c];
                    ox += Math.Pow(r - ox, 2) * curr;
                    oy += Math.Pow(c - oy, 2) * curr;
                }
            }

            // TODO: check what to do when ox or oy reach the max value (NaN)

            ox = Math.Sqrt(ox);
            oy = Math.Sqrt(oy);
        }

        /// <summary>
        /// Computes the entropy of Px.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The Px entropy.</returns>
        public static double PxEntropy(Matrix<double> matrix)
        {
            int rows = matrix.Rows;
            double HX = 0.0;
            for (int i = 0; i < rows; i++)
            {
                var px = Px(matrix, i);
                if (px > 0)
                {
                    HX += px * Math.Log(px);
                }
            }
            return -HX;
        }

        /// <summary>
        /// Computes the entropy of Py.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The Py entropy.</returns>
        public static double PyEntropy(Matrix<double> matrix)
        {
            int cols = matrix.Cols;
            double HY = 0.0;
            for (int j = 0; j < cols; j++)
            {
                var py = Py(matrix, j);
                if (py > 0)
                {
                    HY += py * Math.Log(py);
                }
            }
            return -HY;
        }
        #endregion

        #region HaralickFeatures
        /// <summary>
        /// Computes the matrix Angular Second Moment (f1).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Angular Second Moment.</returns>
        public static double ComputeAngularSecondMoment(Matrix<double> matrix)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double angularSecondMoment = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    angularSecondMoment += Math.Pow(matrix[r, c], 2);
                }
            }
            return angularSecondMoment;
        }

        /// <summary>
        /// Computes the matrix Contrast (f2).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Contrast</returns>
        public static double ComputeContrast(Matrix<double> matrix)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double contrast = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var d = r - c;
                    contrast += Math.Pow(d, 2) * matrix[r, c];
                }
            }
            return contrast;
        }

        /// <summary>
        /// Computes the Correlation Coefficient (f3).
        /// Shows the linear dependency of gray level values in the cooccurrence matrix.
        /// N.B. to compute this mean of px and py (µx, µy) are needed, so they will be computed
        /// https://www.hindawi.com/journals/ijbi/2015/267807/
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The Correlation Coefficient.</returns>
        public static double ComputeCorrelationCoefficient(Matrix<double> matrix)
        {
            Mean(matrix, out var µx, out var µy);
            return ComputeCorrelationCoefficient(matrix, µx, µy);
        }

        /// <summary>
        /// Computes the matrix Correlation Coefficient (f3).
        /// Shows the linear dependency of gray level values in the cooccurrence matrix.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="µx">Mean of px</param>
        /// <param name="µy">Mean of py</param>
        /// <returns>The matrix Correlation Coefficient.</returns>
        public static double ComputeCorrelationCoefficient(Matrix<double> matrix, double µx, double µy)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            // standard deviation
            double ox = 0.0;
            double oy = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var curr = matrix[r, c];
                    ox += Math.Pow(r - ox, 2) * curr;
                    oy += Math.Pow(c - oy, 2) * curr;
                }
            }

            // Reached max value, we return 0 because the result would be divided by their product
            if (Double.IsNaN(ox) || Double.IsNaN(oy))
            {
                return 0.0;
            }

            ox = Math.Sqrt(ox);
            oy = Math.Sqrt(oy);

            double correlationCoefficient = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    // If both 0, we divide for a small value
                    if (ox == 0.0 || oy == 0.0)
                    {
                        correlationCoefficient += matrix[r, c] * (r - µx) * (c - µy) / 0.03125;
                    }
                    else
                    {
                        correlationCoefficient += matrix[r, c] * (r - µx) * (c - µy) / (ox * oy);
                    }

                }
            }

            return Double.IsNaN(correlationCoefficient) ? Double.MaxValue : correlationCoefficient;
        }

        /// <summary>
        /// Computes the matrix Sums of Squares: Variance (f4).
        /// N.B. to compute this mean (µ) is needed, so it will be computed
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Sums of Squares.</returns>
        public static double ComputeSumsOfSquares(Matrix<double> matrix)
        {
            var µ = Mean(matrix, out var µx, out var µy);
            return ComputeSumsOfSquares(matrix, µ);
        }

        /// <summary>
        /// Computes the matrix Sums of Squares: Variance (f4).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="µ">The mean value</param>
        /// <returns>The matrix Sums of Squares.</returns>
        public static double ComputeSumsOfSquares(Matrix<double> matrix, double µ)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double variance = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    variance += Math.Pow(r - µ, 2) * matrix[r, c];
                }
            }
            return variance;
        }

        /// <summary>
        /// Computes the matrix Inverse Difference Moment (f5).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Inverse Difference Moment.</returns>
        public static double ComputeInverseDifferenceMoment(Matrix<double> matrix)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;

            double inverseDifferenceMoment = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    inverseDifferenceMoment += matrix[r, c] / (1.0 + Math.Pow(r - c, 2));
                }
            }
            return inverseDifferenceMoment;
        }

        /// <summary>
        /// Computes the matrix Sum Average (f6).
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>The matrix Sum Average.</returns>
        public static double ComputeSumAverage(Matrix<double> matrix)
        {
            double sumAverage = 0.0;
            for (int i = 2; i < matrix.Rows * 2; i++)
            {
                sumAverage += i * Pxplusy(matrix, i);
            }
            return sumAverage;
        }

        /// <summary>
        /// Computes the matrix Sum Variance (f7).
        /// N.B. to do this Sum Entropy (f8) is needed, so it will be computed
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns></returns>
        public static double ComputeSumVariance(Matrix<double> matrix)
        {
            double sumEntropy = ComputeSumEntropy(matrix);
            return ComputeSumVariance(matrix, sumEntropy);
        }

        /// <summary>
        /// Computes the matrix Sum Variance (f7).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="sumEntropy">The Sum Entropy (f8) result</param>
        /// <returns>The matrix Sum Variance.</returns>
        public static double ComputeSumVariance(Matrix<double> matrix, double sumEntropy)
        {
            double sumVariance = 0.0;
            for (int i = 2; i < matrix.Rows * 2; i++)
            {
                sumVariance += Math.Pow(i - sumEntropy, 2) * Pxplusy(matrix, i);
            }
            return sumVariance;
        }

        /// <summary>
        /// Computes the matrix Sum Entropy (f8).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Sum Entropy.</returns>
        public static double ComputeSumEntropy(Matrix<double> matrix)
        {
            double sumEntropy = 0.0;
            for (int i = 2; i < matrix.Rows * 2; i++)
            {
                var current = Pxplusy(matrix, i);
                if (current > 0)
                {
                    sumEntropy += current * Math.Log(current);
                }
            }
            return -sumEntropy;
        }

        /// <summary>
        /// Computes the matrix Entropy (f9).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Entrophy.</returns>
        public static double ComputeEntropy(Matrix<double> matrix)
        {
            double entropy = 0.0;
            var rows = matrix.Rows;
            var cols = matrix.Cols;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var current = matrix[r, c];
                    if (current > 0)
                    {
                        entropy += current * Math.Log(current);
                    }
                }
            }
            return -entropy;
        }

        /// <summary>
        /// Computes the matrix Difference Variance (f10).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Difference Variance.</returns>
        public static double ComputeDifferenceVariance(Matrix<double> matrix)
        {
            double differenceVariance = 0.0;
            for (int i = 0; i < matrix.Rows - 1; i++)
            {
                differenceVariance += Math.Pow(i, 2) * Pxminusy(matrix, i);
            }
            return differenceVariance;
        }

        /// <summary>
        /// Computes the matrix Difference Entropy (f11).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Difference Entropy.</returns>
        public static double ComputeDifferenceEntropy(Matrix<double> matrix)
        {
            double differenceEntropy = 0.0;
            for (int i = 0; i < matrix.Rows - 1; i++)
            {
                var curr = Pxminusy(matrix, i);
                if (curr > 0)
                {
                    differenceEntropy += curr * Math.Log(curr);
                }
            }
            return -differenceEntropy;
        }

        /// <summary>
        /// Computes the matrix Information Measures of Correlation (f12, f13).
        /// N.B to do this Entropy (f9) is required, so it will be computed
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>A tuple with f12 in Item1 and f13 in Item2.</returns>
        public static Tuple<double, double> ComputeInformationMeasureOfCorrelation(Matrix<double> matrix)
        {
            double entropy = ComputeEntropy(matrix);
            return ComputeInformationMeasureOfCorrelation(matrix, entropy);
        }

        /// <summary>
        /// Computes the matrix Information Measures of Correlation (f12, f13).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="entropy">The Entropy (f9).</param>
        /// <returns>A tuple with f12 in Item1 and f13 in Item2.</returns>
        public static Tuple<double, double> ComputeInformationMeasureOfCorrelation(Matrix<double> matrix, double entropy)
        {
            int rows = matrix.Rows;
            int cols = matrix.Cols;
            double infoMeasureOfCorrelation1 = 0.0;
            double infoMeasureOfCorrelation2 = 0.0;

            // entropy of px
            double HX = PxEntropy(matrix);

            // entropy of py
            double HY = PyEntropy(matrix);

            double HXY = entropy;

            double HXY1 = 0.0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var curr = matrix[r, c];
                    var pxpy = Px(matrix, r) * Py(matrix, c);
                    if (curr != 0 && pxpy > 0)
                    {
                        HXY1 += curr * Math.Log(pxpy);
                    }
                }
            }
            HXY1 = -HXY1;

            double HXY2 = 0.0;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    var pxpy = Px(matrix, i) * Py(matrix, j);
                    if (pxpy > 0)
                    {
                        HXY2 += pxpy * Math.Log(pxpy);
                    }
                }
            }
            HXY2 = -HXY2;

            infoMeasureOfCorrelation1 = (HXY - HXY1) / Math.Max(HX, HY);
            infoMeasureOfCorrelation2 = Math.Sqrt(1.0 - Math.Exp(-2 * (HXY2 - HXY)));

            return new Tuple<double, double>(infoMeasureOfCorrelation1, infoMeasureOfCorrelation2);
        }

        /// <summary>
        /// Computes the matrix Maximal Correlation Coefficient (f14).
        /// [MurphyLab] says "the maximal correlation coefficient was not calculated due to computational instability".
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The matrix Maximal Correlation Coefficient</returns>
        public static double ComputeMaxCorrelationCoefficient(Matrix<double> matrix)
        {
            throw new NotImplementedException("TODO: fix qij calculation");

            /*
            int rows = matrix.Rows;
            int cols = matrix.Cols;
       
            double qij;
            double max = 0.0;
            double secondMax = 0.0;

            var k = 0; // should go from 0 to gray levels number (256) - 1 ???
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    var pxpy = Px(matrix, i) * Py(matrix, k);
                    if (pxpy != 0)
                    {
                        // TODO: check what is k
                        qij = (matrix[i, k] * matrix[j, k]) / pxpy;
                        if (qij > max)
                        {
                            secondMax = max;
                            max = qij;
                        }
                        else if (qij > secondMax)
                        {
                            secondMax = qij;
                        }
                    }
                    k++;
                }
                k = 0;
            }
            return Math.Sqrt(secondMax);
            */
        }
        #endregion

        #region RelatedFeatures
        /// <summary>
        /// Computes the energy. The energy is the square root of Angular Second Moment (f1).
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The energy of the matrix.</returns>
        public static double ComputeEnergy(Matrix<double> matrix)
        {
            double angularSecondMoment = ComputeAngularSecondMoment(matrix);
            return ComputeEnergy(matrix, angularSecondMoment);
        }

        /// <summary>
        /// Computes the energy.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <param name="angularSecondMoment">The Angular Second Moment (f1).</param>
        /// <returns>The energy of the matrix.</returns>
        public static double ComputeEnergy(Matrix<double> matrix, double angularSecondMoment)
        {
            return Math.Sqrt(angularSecondMoment);
        }

        /// <summary>
        /// Computes the homogeneity.
        /// </summary>
        /// <param name="matrix">The co-occurrence matrix.</param>
        /// <returns>The homogeneity of the matrix.</returns>
        public static double ComputeHomogeneity(Matrix<double> matrix)
        {
            double homogeneity = 0.0;
            for (int r = 0; r < matrix.Rows; r++)
            {
                for (int c = 0; c < matrix.Cols; c++)
                {
                    homogeneity += matrix[r, c] / (1.0 + Math.Abs(r - c));
                }
            }
            return homogeneity;
        }
        #endregion
    }
}
