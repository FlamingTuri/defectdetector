﻿using Emgu.CV;
using System;
using System.Linq;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides math related stuff.
    /// </summary>
    public class MathUtils
    {
        /// <summary>
        /// Computes the gaussian distribution value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="mean"></param>
        /// <param name="variance"></param>
        /// <returns>The gaussian distribution value.</returns>
        public static double CalculateGaussianDistributionValue(double value, double mean, double variance)
        {
            if (variance == 0)
            {
                return 0;
            }
            var standardDeviation = Math.Sqrt(variance);
            var expPart = -(Math.Pow(value - mean, 2) / (2 * variance));
            var den = standardDeviation * Math.Sqrt(2 * Math.PI);
            return Math.Exp(expPart) / den;
        }

        /// <summary>
        /// Computes the gaussian distribution value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="mean"></param>
        /// <param name="variance"></param>
        /// <returns>The gaussian distribution value.</returns>
        public static double[] CalculateGaussianDistributionValue(double[] value, double[] mean, double[] variance)
        {
            var result = new double[value.Count()];
            for (int i = 0; i < value.Count(); i++)
            {
                result[i] = CalculateGaussianDistributionValue(value[i], mean[i], variance[i]);
            }
            return result;
        }

        /// <summary>
        /// Computes the gaussian distribution value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="mean"></param>
        /// <param name="variance"></param>
        /// <returns>The gaussian distribution value.</returns>
        public static Matrix<double> CalculateGaussianDistributionValue(Matrix<double> value, Matrix<double> mean, Matrix<double> variance)
        {
            var result = new Matrix<double>(value.Rows, value.Cols);
            for (int r = 0; r < value.Rows; r++)
            {
                for (int c = 0; c < value.Cols; c++)
                {
                    result[r, c] = CalculateGaussianDistributionValue(value[r, c], mean[r, c], variance[r, c]);
                }
            }
            return result;
        }
    }
}
