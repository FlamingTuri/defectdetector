﻿using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.XFeatures2D;
using System.Drawing;
using System;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class that wraps Emgu methods. In particular wraps SIFT and SURF descriptors 
    /// and a method to draw a VectorOfKeyPoint structure on an image.
    /// </summary>
    public class FeatureExtractorUtils
    {
        /// <summary>
        /// Detect keypoints and compute descriptors using the SIFT algorithm.
        /// </summary>
        /// <param name="img">The input grayscaled image</param>
        /// <param name="keyPoints">The detected keypoints.</param>
        /// <param name="descriptors">The computed descriptors.</param>
        /// <returns>The descriptors.</returns>
        public static Mat ComputeDescriptorsSIFT(Image<Gray, byte> img, out VectorOfKeyPoint keyPoints)
        {
            throw new EntryPointNotFoundException("EmguCV nuget package doesn't come with support for SIFT descriptor!");
#if SURF_SIFT
            SIFT sift = new SIFT();

            keyPoints = new VectorOfKeyPoint();
            Mat modelDescriptors = new Mat();

            sift.DetectAndCompute(img, null, keyPoints, modelDescriptors, false);
            return modelDescriptors;
#endif
        }

        /// <summary>
        ///  Detect keypoints and compute descriptors using the SURF algorithm.
        /// </summary>
        /// <param name="img">The input grayscaled image.</param>
        /// <param name="hessianThreshold">The threshold that determines how large the output from the Hessian filter must be in order for a point to be used as an interest point.</param>
        /// <param name="keyPoints">The detected keypoints.</param>
        /// <param name="descriptors">The computed descriptors.</param>
        /// <returns>The descriptors.</returns>
        public static Mat ComputeDescriptorsSURF(Image<Gray, byte> img, double hessianThreshold, out VectorOfKeyPoint keyPoints)
        {
            throw new EntryPointNotFoundException("EmguCV nuget package doesn't come with support for SURF descriptor!");
#if SURF_SIFT
            SURF surf = new SURF(hessianThreshold);

            keyPoints = new VectorOfKeyPoint();
            Mat modelDescriptors = new Mat();

            surf.DetectAndCompute(img, null, keyPoints, modelDescriptors, false);
            return modelDescriptors;
#endif
        }

        /// <summary>
        /// Draws the given keypoints on the selected image. 
        /// </summary>
        /// <param name="image">The input image.</param>
        /// <param name="keyPoints">The image keypoints.</param>
        /// <returns>The image with the drawn keypoints.</returns>
        public static Mat DrawKeypoints(IInputArray image, VectorOfKeyPoint keyPoints)
        {
            Mat imageWithKeypoins = new Mat();
            Features2DToolbox.DrawKeypoints(image, keyPoints, imageWithKeypoins, new Bgr(Color.Red));
            return imageWithKeypoins;
        }
    }
}
