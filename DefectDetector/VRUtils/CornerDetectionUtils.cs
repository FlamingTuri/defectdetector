﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace DefectDetector.Utils
{
    /// <summary>
    /// Static class which provides useful methods for corner detection.
    /// </summary>
    public class CornerDetectionUtils
    {
        /// <summary>
        /// Computes the cornerness map.
        /// </summary>
        /// <param name="image">The input grayscale image.</param>
        /// <param name="xApertureSize">Size of the x aperture.</param>
        /// <param name="yApertureSize">Size of the y aperture.</param>
        /// <param name="filterSize">Size of the filter.</param>
        /// <param name="sigma">The sigma.</param>
        /// <param name="alfa">The alfa.</param>
        /// <param name="cornernessThr">The cornerness threshold.</param>
        /// <returns>The cornerness map.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">if xApertureSize or yApertureSize are out of range.</exception>
        public static Image<Gray, double> ComputeCornernessMap(Image<Gray, byte> image,
                                                               int xApertureSize = 3,
                                                               int yApertureSize = 3,
                                                               int filterSize = 7,
                                                               double sigma = 1.4,
                                                               double alfa = 0.04,
                                                               int cornernessThr = 1000000)
        {
            if (xApertureSize < 1 || yApertureSize < 1)
            {
                throw new ArgumentOutOfRangeException("xApertureSize || yApertureSize", "xApertureSize or yApertureSize params must be > 0");
            }

            // Gradient computation with Sobel 3x3
            var gx = image.Sobel(1, 0, xApertureSize);
            var gY = image.Sobel(0, 1, yApertureSize);

            var gx2 = new Image<Gray, float>(image.Width, image.Height);
            var gy2 = new Image<Gray, float>(image.Width, image.Height);
            var gxy = new Image<Gray, float>(image.Width, image.Height);

            Parallel.For(0, image.Height, y =>
            {
                for (int x = 0; x < image.Width; x++)
                {
                    gx2.Data[y, x, 0] = gx.Data[y, x, 0] * gx.Data[y, x, 0];
                    gy2.Data[y, x, 0] = gY.Data[y, x, 0] * gY.Data[y, x, 0];
                    gxy.Data[y, x, 0] = gx.Data[y, x, 0] * gY.Data[y, x, 0];
                } 
            });

            // Gaussian smoothing
            gx2 = gx2.SmoothGaussian(filterSize, filterSize, sigma, sigma);
            gy2 = gy2.SmoothGaussian(filterSize, filterSize, sigma, sigma);
            gxy = gxy.SmoothGaussian(filterSize, filterSize, sigma, sigma);

            // Compute cornerness
            var cornernessMap = new Image<Gray, double>(gxy.Width, gxy.Height);
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    var m = new Matrix<double>(2, 2);
                    m[0, 0] = gx2.Data[y, x, 0];
                    m[0, 1] = m[1, 0] = gxy.Data[y, x, 0];
                    m[1, 1] = gy2.Data[y, x, 0];

                    var tr = m[0, 0] + m[1, 1];
                    var det = m[0, 0] * m[1, 1] - m[1, 0] * m[0, 1];
                    var val = det - alfa * tr * tr;
                    if (val > cornernessThr)
                        cornernessMap.Data[y, x, 0] = val;
                }
            }
            return cornernessMap;
        }

        /// <summary>
        /// Computes the non-maxima suppression.
        /// </summary>
        /// <param name="cornernessMap">The cornerness map.</param>
        /// <param name="R">The cornerness coefficient.</param>
        /// <returns>The list of local maximum points.</returns>
        public static List<Point> ComputeNonMaximaSuppression(Image<Gray, double> cornernessMap, int R = 3)
        {
            var pointList = new List<Point>();

            // Non-maxima suppression
            var maxY = cornernessMap.Height - R;
            var maxX = cornernessMap.Width - R;

            for (int y = R; y < maxY; y++)
            {
                for (int x = R; x < maxX; x++)
                {
                    double currentValue = cornernessMap[y, x].Intensity;

                    for (int i = -R; (currentValue != 0) && (i <= R); i++)
                    {
                        // for each windows' pixel
                        for (int j = -R; j <= R; j++)
                        {
                            if (cornernessMap[y + i, x + j].Intensity > currentValue)
                            {
                                currentValue = 0;
                                break;
                            }
                        }
                    }

                    // check if this point is really interesting
                    if (currentValue != 0)
                    {
                        pointList.Add(new Point(x, y));
                    }
                }
            }
            return pointList;
        }

        /// <summary>
        /// Computes the non-maxima suppressed cornerness map.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="xApertureSize">Size of the x aperture.</param>
        /// <param name="yApertureSize">Size of the y aperture.</param>
        /// <param name="filterSize">Size of the filter.</param>
        /// <param name="sigma">The sigma.</param>
        /// <param name="alfa">The alfa.</param>
        /// <param name="cornernessThr">The cornerness threshold.</param>
        /// <param name="R">The cornerness coefficient.</param>
        /// <returns>The list of local maximum points.</returns>
        public static List<Point> ComputeNonMaximaSuppressedCornernessMap(Image<Gray, byte> image,
                                                                      int xApertureSize = 3,
                                                                      int yApertureSize = 3,
                                                                      int filterSize = 7,
                                                                      double sigma = 1.4,
                                                                      double alfa = 0.04,
                                                                      int cornernessThr = 1000000,
                                                                      int R = 3)
        {
            var cornernessMap = ComputeCornernessMap(image, xApertureSize, yApertureSize, filterSize, sigma, alfa, cornernessThr);
            var corners = ComputeNonMaximaSuppression(cornernessMap, R);
            return corners;
        }
    }
}
