﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides useful methods for gradient computation.
    /// </summary>
    public class GradientUtils
    {
        /// <summary>
        /// Computes the gradient.
        /// </summary>
        /// <param name="img">The input grayscale image.</param>
        /// <param name="xGradient">The x gradient.</param>
        /// <param name="yGradient">The y gradient.</param>
        /// <param name="modGradient">The mod gradient.</param>
        /// <param name="averageGradient">The average gradient.</param>
        /// <param name="xApertureSize">Size of the x aperture.</param>
        /// <param name="yApertureSize">Size of the y aperture.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"> if xApertureSize or yApertureSize are out of range.</exception>
        public static void ComputeGradient(Image<Gray, byte> img,
                                           out Image<Gray, float> xGradient,
                                           out Image<Gray, float> yGradient,
                                           out Image<Gray, float> modGradient,
                                           out double averageGradient,
                                           int xApertureSize = 3,
                                           int yApertureSize = 3)
        {
            if (xApertureSize < 1 || yApertureSize < 1)
            {
                throw new ArgumentOutOfRangeException("xApertureSize || yApertureSize", "xApertureSize or yApertureSize params must be > 0");
            }

            // Gradient on the x dimension
            xGradient = img.Sobel(1, 0, xApertureSize);
            // Gradient on the y dimension
            yGradient = img.Sobel(0, 1, yApertureSize);

            var width = img.Width;
            var height = img.Height;
            // Gradient magnitude and average
            modGradient = new Image<Gray, float>(width, height);
            averageGradient = 0.0;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var grad = Math.Pow(xGradient[y, x].Intensity, 2) + Math.Pow(yGradient[y, x].Intensity, 2);
                    modGradient[y, x] = new Gray(grad);
                    averageGradient += grad;
                }
            }
            averageGradient /= width * height;
        }

        /// <summary>
        /// Computes the gradient segmentation.
        /// </summary>
        /// <param name="img">The input grayscale image.</param>
        /// <param name="bSize">The block size (window).</param>
        /// <param name="thr">The segmentation threshold.</param>
        /// <param name="xGradient">The x gradient.</param>
        /// <param name="yGradient">The y gradient.</param>
        /// <param name="modGradient">The average gradient.</param>
        /// <returns>The mask of the filtered gradient values.</returns>
        public static Image<Gray, byte> GradientSegmentation(Image<Gray, byte> img,
                                                             int bSize,
                                                             float thr,
                                                             out Image<Gray, float> xGradient,
                                                             out Image<Gray, float> yGradient,
                                                             out Image<Gray, float> modGradient)
        {
            // Moving window step
            int bStep = bSize / 2;

            // Calculate gradient
            ComputeGradient(img, out xGradient, out yGradient, out modGradient, out double averageGradient);

            // Segmentation threshold
            var thrValue = averageGradient * thr;

            // Calculate segmentated image, with no morphological operators applied
            var segmentatedImage = new Image<Gray, byte>(img.Width, img.Height);

            for (int y = 0; y < img.Height - bSize - 1; y += bStep)
            {
                for (int x = 0; x < img.Width - bSize - 1; x += bStep)
                {
                    // Calculate local gradient average
                    var gb = modGradient.GetSubRect(new Rectangle(x, y, bSize, bSize)).GetAverage().Intensity;
                    if (gb >= thrValue)
                    {
                        for (int i = 0; i < bSize; i++)
                        {
                            for (int j = 0; j < bSize; j++)
                            {
                                segmentatedImage[y + i, x + j] = new Gray(255);
                            }
                        }
                    }
                }
            }

            return segmentatedImage;
        }

        /// <summary>
        /// Computes the gradient segmentation.
        /// </summary>
        /// <param name="img">The input grayscale image.</param>
        /// <param name="bSize">The block size (window).</param>
        /// <param name="thr">The segmentation threshold.</param>
        /// <returns>The mask of the filtered gradient values.</returns>
        public static Image<Gray, byte> GradientSegmentation(Image<Gray, byte> img, int bSize, float thr)
        {
            //Ignoring the gradient
            var xG = new Image<Gray, float>(img.Width, img.Height);
            var yG = new Image<Gray, float>(img.Width, img.Height);
            var modG = new Image<Gray, float>(img.Width, img.Height);
            return GradientSegmentation(img, bSize, thr, out xG, out yG, out modG);
        }
    }
}
