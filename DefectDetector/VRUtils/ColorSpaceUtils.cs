﻿using Emgu.CV;
using Emgu.CV.Structure;

namespace DefectDetector.VRUtils
{
    /// <summary>
    /// Static class which provides useful methods for color space manipulation.
    /// </summary>
    public class ColorSpaceUtils
    {
        #region Color Space Conversion
        /// <summary>
        /// Converts the image from RGB color space to YCbCr.
        /// </summary>
        /// <param name="img">The input image.</param>
        /// <returns>The input image in the YCbCr color space.</returns>
        public static Image<Ycc, byte> ConvertToYCbCr(Image<Bgr, byte> img)
        {
            return img.Convert<Ycc, byte>();
        }

        /// <summary>
        /// Converts the image from RGB color space to HSV.
        /// </summary>
        /// <param name="img">The img.</param>
        /// <returns>The input image in the HSV color space.</returns>
        public static Image<Hsv, byte> ConvertToHSV(Image<Bgr, byte> img)
        {
            return img.Convert<Hsv, byte>();
        }
        #endregion

        #region Color Channel Splitting
        /// <summary>
        /// Splits the color channels of a RGB image.
        /// </summary>
        /// <param name="img">The input image.</param>
        /// <param name="R">The red channel.</param>
        /// <param name="G">The green channel.</param>
        /// <param name="B">The blue channel.</param>
        public static void SplitChannels(Image<Bgr, byte> img,
                                         out Image<Gray, byte> R,
                                         out Image<Gray, byte> G,
                                         out Image<Gray, byte> B)
        {
            var channels = img.Split();
            B = channels[0]; // blue
            G = channels[1]; // green
            R = channels[2]; // red
        }

        /// <summary>
        /// Splits the color channels of a YCbCr image.
        /// </summary>
        /// <param name="img">The input image.</param>
        /// <param name="Y">The y.</param>
        /// <param name="Cb">The cb.</param>
        /// <param name="Cr">The cr.</param>
        public static void SplitChannels(Image<Ycc, byte> img,
                                         out Image<Gray, byte> Y,
                                         out Image<Gray, byte> Cb,
                                         out Image<Gray, byte> Cr)
        {
            var channels = img.Split();
            Y = channels[0]; // Luma
            // Note: Cr is before Cb
            Cr = channels[1]; // red-difference
            Cb = channels[2]; // blue-difference
        }

        /// <summary>
        /// Splits the color channels of a HSV image.
        /// </summary>
        /// <param name="img">The input image.</param>
        /// <param name="Hue">The hue.</param>
        /// <param name="Saturation">The saturation.</param>
        /// <param name="Value">The value.</param>
        public static void SplitChannels(Image<Hsv, byte> img,
                                         out Image<Gray, byte> Hue,
                                         out Image<Gray, byte> Saturation,
                                         out Image<Gray, byte> Value)
        {
            var channels = img.Split();
            Hue = channels[0];
            Saturation = channels[1];
            Value = channels[2];
        }

        #endregion
    }
}
