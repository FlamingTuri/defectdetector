﻿using System;

namespace DefectDetector.Models
{
    /// <summary>
    /// The arguments for the detector's progress event.
    /// </summary>
    public class DetectorProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Execution time.
        /// </summary>
        public long ExecutionTime { get; }

        /// <summary>
        /// The default constructor.
        /// </summary>
        /// <param name="executionTime">The execution time</param>
        public DetectorProgressEventArgs(long executionTime)
        {
            this.ExecutionTime = executionTime;
        }
    }
}
