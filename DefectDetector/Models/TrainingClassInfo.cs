﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace DefectDetector.Models
{
    /// <summary>
    /// The training class information
    /// </summary>
    public class TrainingClassInfo : IDisposable
    {
        private const string JSON_MIN = "min";
        private const string JSON_MAX = "max";
        private const string JSON_COUNT = "count";
        private const string JSON_MEAN = "mean";
        private const string JSON_VARIANCE = "variance";
        private const string JSON_SAMPLE_VARIANCE = "sampleVariance";

        /// <summary>
        /// The number of samples
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// The minimum values.
        /// </summary>
        public double[] Min { get; set; }

        /// <summary>
        /// The maximum values.
        /// </summary>
        public double[] Max { get; set; }

        /// <summary>
        /// The mean values.
        /// </summary>
        public double[] Mean { get; set; }

        /// <summary>
        /// The variance values.
        /// </summary>
        public double[] Variance { get; set; }

        /// <summary>
        /// The sample variance values.
        /// </summary>
        public double[] SampleVariance { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="featuresNumber">The number of features</param>
        public TrainingClassInfo(int featuresNumber)
        {
            this.Count = 0;
            this.Min = new double[featuresNumber];
            this.Max = new double[featuresNumber];
            this.Mean = new double[featuresNumber];
            this.Variance = new double[featuresNumber];
            this.SampleVariance = new double[featuresNumber];
        }

        /// <summary>
        /// The secondary constructor.
        /// </summary>
        /// <param name="count">The number of samples</param>
        /// <param name="min">The min values</param>
        /// <param name="max">The max values</param>
        /// <param name="mean">The mean values</param>
        /// <param name="variance">The variance values</param>
        /// <param name="sampleVariance">The sample variance</param>
        public TrainingClassInfo(int count, double[] min, double[] max, double[] mean, double[] variance, double[] sampleVariance)
        {
            this.Count = count;
            this.Min = min;
            this.Max = max;
            this.Mean = mean;
            this.Variance = variance;
            this.SampleVariance = sampleVariance;
        }

        /// <summary>
        /// Adds a new feature set.
        /// </summary>
        /// <param name="featureSet">The feature set</param>
        public void AddFeatureSet(double[] featureSet)
        {
            //Updating count
            Count = Count + 1;
            //Computing delta to update mean
            for (int i = 0; i < featureSet.Count(); i++)
            {
                var delta = featureSet[i] - Mean[i];
                Mean[i] += delta / Count;
                var delta2 = featureSet[i] - Mean[i];
                var meanVariance = delta * delta2;
                // Variance
                Variance[i] += meanVariance / Count;
                // SampleVariance (if count > 1)
                if (Count > 1)
                {
                    SampleVariance[i] += meanVariance / (Count - 1);
                }
                // Min and Max
                if (featureSet[i] > Max[i])
                {
                    Max[i] = featureSet[i];
                }
                if (featureSet[i] < Min[i])
                {
                    Min[i] = featureSet[i];
                }
            }
        }

        /// <summary>
        /// Loads data from a json object.
        /// </summary>
        /// <param name="json">The JObject</param>
        public void LoadJson(JObject json)
        {
            Count = (int) json.Property(JSON_COUNT).Select(i => (int)i).First();
            Min = json.Property(JSON_MIN).First.Select(i => (double)i).ToArray();
            Max = json.Property(JSON_MAX).First.Select(i => (double)i).ToArray();
            Mean = json.Property(JSON_MEAN).First.Select(i => (double)i).ToArray();
            Variance = json.Property(JSON_VARIANCE).First.Select(i => (double)i).ToArray();
            SampleVariance = json.Property(JSON_SAMPLE_VARIANCE).First.Select(i => (double)i).ToArray();
        }

        /// <summary>
        /// Serializes the instance into a json object.
        /// </summary>
        /// <returns>The JObject</returns>
        public JObject ToJson()
        {
            var json = new JObject(
                    new JProperty(JSON_COUNT, Count),
                    new JProperty(JSON_MIN, new JArray(Min)),
                    new JProperty(JSON_MAX, new JArray(Max)),
                    new JProperty(JSON_MEAN, new JArray(Mean)),
                    new JProperty(JSON_VARIANCE, new JArray(Variance)),
                    new JProperty(JSON_SAMPLE_VARIANCE, new JArray(SampleVariance))
                );

            return json;
        }

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            Min = null;
            Max = null;
            Mean = null;
            Variance = null;
            SampleVariance = null;
        }
    }
}
