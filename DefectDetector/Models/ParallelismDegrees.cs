﻿namespace DefectDetector.Models
{
    /// <summary>
    /// Supported degrees of parallelism
    /// </summary>
    public enum ParallelismDegrees
    {
        /// <summary>
        /// Serial execution
        /// </summary>
        None,
        /// <summary>
        /// Mixed execution, serial execution with concurrent sub-operations
        /// </summary>
        Hybrid,
        /// <summary>
        /// Parallel execution
        /// </summary>
        Low,
        /// <summary>
        /// Limited asynchronous task-oriented execution (limited number of tasks)
        /// </summary>
        Medium,
        /// <summary>
        /// Asynchronous task-oriented execution
        /// </summary>
        High
    }
}
