﻿using System.Collections.Generic;
using System.Linq;

namespace DefectDetector.Models
{
    /// <summary>
    /// The steps of the window dimension to use.
    /// </summary>
    public class DimensionSteps
    {
        /// <summary>
        /// The steps.
        /// </summary>
        public List<int> Steps { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="steps">The steps.</param>
        public DimensionSteps(params int[] steps)
        {
            this.Steps = steps.ToList();
        }
    }
}
