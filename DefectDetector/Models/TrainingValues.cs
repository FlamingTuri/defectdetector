﻿using Emgu.CV;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DefectDetector.Models
{
    /// <summary>
    /// Useful training values container
    /// </summary>
    class TrainingValues : IDisposable
    {
        private const string JSON_MIN_VALS = "minVals";
        private const string JSON_MAX_VALS = "maxVals";
        private const string JSON_FEATURES_SETS = "featureSets";
        private const string JSON_LABEL = "label";
        private const string JSON_DATA = "data";

        /// <summary>
        /// Dictionary holding the number of feature and the overall feature sets info for each label class
        /// </summary>
        private Dictionary<int, TrainingClassInfo> ClassFeaturesSets;

        /// <summary>
        /// Vector containing the maximum feature values found during training
        /// </summary>
        public double[] MinVals { get; set; }

        /// <summary>
        /// Vector containing the maximum feature values found during training
        /// </summary>
        public double[] MaxVals { get; set; }

        /// <summary>
        /// Number of training features
        /// </summary>
        public int NumFeatures { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public TrainingValues()
        {
            ClassFeaturesSets = new Dictionary<int, TrainingClassInfo>();
        }

        /// <summary>
        /// Adds a new feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <param name="featureSet">The feature set</param>
        public void AddFeatureSet(int classLabel, double[] featureSet)
        {
            if (ClassFeaturesSets.ContainsKey(classLabel))
            {
                // Label found, we update the training info
                var currentInfo = ClassFeaturesSets[classLabel];
                currentInfo.AddFeatureSet(featureSet);
            } else
            {
                // New label, we add a new set
                var trainingInfo = new TrainingClassInfo(featureSet.Count());
                ClassFeaturesSets.Add(classLabel, trainingInfo);
            }
        }

        /// <summary>
        /// Gets the mean feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The mean feature set</returns>
        public double[] GetMeanFeatureSet(int classLabel)
        {
            if (ClassFeaturesSets.ContainsKey(classLabel))
            {
                // Label found, we retrieve the mean
                var trainingInfo = ClassFeaturesSets[classLabel];
                return trainingInfo.Mean;
            }
            else
            {
                // New label, we return an empty array
                return new double[NumFeatures];
            }
        }

        /// <summary>
        /// Gets the variance feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The variance feature set</returns>
        public double[] GetVarianceFeatureSet(int classLabel)
        {
            if (ClassFeaturesSets.ContainsKey(classLabel))
            {
                // Label found, we retrieve the mean
                var trainingInfo = ClassFeaturesSets[classLabel];
                return trainingInfo.Variance;
            }
            else
            {
                // New label, we return an empty array
                return new double[NumFeatures];
            }
        }

        /// <summary>
        /// Gets the sample variance feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The sample variance feature set</returns>
        public double[] GetSampleVarianceFeatureSet(int classLabel)
        {
            if (ClassFeaturesSets.ContainsKey(classLabel))
            {
                // Label found, we retrieve the mean
                var trainingInfo = ClassFeaturesSets[classLabel];
                return trainingInfo.SampleVariance;
            }
            else
            {
                // New label, we return an empty array
                return new double[NumFeatures];
            }
        }

        /// <summary>
        /// Gets the mean feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The mean feature set</returns>
        public Matrix<double> GetMeanFeatureSetAsMatrix(int classLabel)
        {
            double[] meanFeatureSet = this.GetMeanFeatureSet(classLabel);
            var matrix = new Matrix<double>(1, NumFeatures);
            for (int i = 0; i < NumFeatures; i++)
            {
                matrix[0, i] = meanFeatureSet[i];
            }
            return matrix;
        }

        /// <summary>
        /// Gets the variance feature set
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The variance feature set</returns>
        public Matrix<double> GetVarianceFeatureSetAsMatrix(int classLabel)
        {
            double[] varianceFeatureSet = this.GetVarianceFeatureSet(classLabel);
            var matrix = new Matrix<double>(1, NumFeatures);
            for (int i = 0; i < NumFeatures; i++)
            {
                matrix[0, i] = varianceFeatureSet[i];
            }
            return matrix;
        }

        /// <summary>
        /// Gets the sample variance class feature sets
        /// </summary>
        /// <param name="classLabel">The class label</param>
        /// <returns>The sample variance feature set.</returns>
        public Matrix<double> GetSampleVarianceFeatureSetAsMatrix(int classLabel)
        {
            double[] varianceSampleFeatureSet = this.GetSampleVarianceFeatureSet(classLabel);
            var matrix = new Matrix<double>(1, NumFeatures);
            for (int i = 0; i < NumFeatures; i++)
            {
                matrix[0, i] = varianceSampleFeatureSet[i];
            }
            return matrix;
        }

        /// <summary>
        /// Gets the mean class feature sets
        /// </summary>
        /// <returns>The mean class feature sets</returns>
        public Dictionary<int, double[]> GetMeanClassFeatureSets()
        {
            var meanDictionary = new Dictionary<int, double[]>();
            foreach (var k in ClassFeaturesSets.Keys)
            {
                var meanFeatureSet = GetMeanFeatureSet(k);
                meanDictionary.Add(k, meanFeatureSet);
            }
            return meanDictionary;
        }

        /// <summary>
        /// Loads data from a json object
        /// </summary>
        /// <param name="json">The JObject</param>
        public void LoadJson(JObject json)
        {
            MinVals = json.Property(JSON_MIN_VALS).First.Select(i => (double)i).ToArray();
            MaxVals = json.Property(JSON_MAX_VALS).First.Select(i => (double)i).ToArray();
            var featureSets = json.Property(JSON_FEATURES_SETS).First;
            foreach (var fs in featureSets.Children<JObject>())
            {
                int label = fs.Property(JSON_LABEL).Select(i => (int)i).First();
                var data = (JObject) fs.Property(JSON_DATA).First();
                var trainingInfo = new TrainingClassInfo(NumFeatures);
                trainingInfo.LoadJson(data);
                ClassFeaturesSets.Add(label, trainingInfo);
            }
        }

        /// <summary>
        /// Serializes the instance into a json object
        /// </summary>
        /// <returns>The JObject</returns>
        public JObject ToJson()
        {
            var featureSets = ClassFeaturesSets.Select(cfs => {
                return new JObject(
                    new JProperty(JSON_LABEL, cfs.Key),
                    new JProperty(JSON_DATA, cfs.Value.ToJson())
                    );
                });

            var json = new JObject(
                    new JProperty(JSON_MIN_VALS, new JArray(MinVals)),
                    new JProperty(JSON_MAX_VALS, new JArray(MaxVals)),
                    new JProperty(JSON_FEATURES_SETS, featureSets)
                );

            return json;
        }

        /// <summary>
        /// Disposes the instance
        /// </summary>
        public void Dispose()
        {
            ClassFeaturesSets.Values.ToList().ForEach(i => i.Dispose());
            ClassFeaturesSets.Clear();
            ClassFeaturesSets = null;
            MinVals = null;
            MaxVals = null;
        }
    }
}
