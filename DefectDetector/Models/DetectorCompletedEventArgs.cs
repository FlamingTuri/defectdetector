﻿using System;
namespace DefectDetector.Models
{
    /// <summary>
    /// The arguments for the detector's completed event.
    /// </summary>
    public class DetectorCompletedEventArgs : EventArgs
    {
        /// <summary>
        /// The total execution time.
        /// </summary>
        public long TotalExecutionTime { get; }

        /// <summary>
        /// The default constructor.
        /// </summary>
        /// <param name="totalExecutionTime">The total execution time</param>
        public DetectorCompletedEventArgs(long totalExecutionTime)
        {
            this.TotalExecutionTime = totalExecutionTime;
        }
    }
}
