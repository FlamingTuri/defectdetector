﻿using Emgu.CV;
using System;

namespace DefectDetector.Models
{
     /// <summary>
     /// The result of the detector test.
     /// </summary>
     public class TestResult : IDisposable
     {
        /// <summary>
        /// The prediction.
        /// </summary>
        public int Prediction;
        /// <summary>
        /// The test feature set.
        /// </summary>
        public Matrix<double> TestFeatureSet { get; }
        /// <summary>
        /// The class mean feature set.
        /// </summary>
        public Matrix<double> ClassMeanFeatureSet { get; }
        /// <summary>
        /// The class variance feature set.
        /// </summary>
        public Matrix<double> ClassVarianceFeatureSet { get; }
        /// <summary>
        /// The class sample variance feature set.
        /// </summary>
        public Matrix<double> ClassSampleVarianceFeatureSet { get; }
        /// <summary>
        /// Whether the result is inside the threshold for variance.
        /// </summary>
        public bool IsInsideThresholdForVariance { get; }
        /// <summary>
        /// Whether the result is inside the threshold for sample variance.
        /// </summary>
        public bool IsInsideThresholdForSampleVariance { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="prediction">The prediction</param>
        /// <param name="testFeatureSet">The test feature set</param>
        /// <param name="classMeanFeatureSet"></param>
        /// <param name="classVarianceFeatureSet"></param>
        /// <param name="classSampleVarianceFeatureSet"></param>
        /// <param name="isInsideThresholdForVariance"></param>
        /// <param name="isInsideThresholdForSampleVariance"></param>
        public TestResult(int prediction, Matrix<double> testFeatureSet, Matrix<double> classMeanFeatureSet, Matrix<double> classVarianceFeatureSet, Matrix<double> classSampleVarianceFeatureSet, bool isInsideThresholdForVariance, bool isInsideThresholdForSampleVariance)
        {
            Prediction = prediction;
            TestFeatureSet = testFeatureSet ?? throw new ArgumentNullException(nameof(testFeatureSet));
            ClassMeanFeatureSet = classMeanFeatureSet ?? throw new ArgumentNullException(nameof(classMeanFeatureSet));
            ClassVarianceFeatureSet = classVarianceFeatureSet ?? throw new ArgumentNullException(nameof(classVarianceFeatureSet));
            ClassSampleVarianceFeatureSet = classSampleVarianceFeatureSet ?? throw new ArgumentNullException(nameof(classSampleVarianceFeatureSet));
            IsInsideThresholdForVariance = isInsideThresholdForVariance;
            IsInsideThresholdForSampleVariance = isInsideThresholdForSampleVariance;
        }

        /// <summary>
        /// Disposes the instance
        /// </summary>
        public void Dispose()
        {
            if (TestFeatureSet != null)
            {
                TestFeatureSet.Dispose();
            }

            if (ClassMeanFeatureSet != null)
            {
                ClassMeanFeatureSet.Dispose();
            }

            if (ClassVarianceFeatureSet != null)
            {
                ClassVarianceFeatureSet.Dispose();
            }

            if (ClassMeanFeatureSet != null)
            {
                ClassSampleVarianceFeatureSet.Dispose();
            }
        }
    }
}
