﻿using DefectDetector.Models;
using DefectDetector.Utils;
using DefectDetector.VRUtils;
using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.MlEnum;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DefectDetector
{
    /// <summary>
    /// Wood defect detector
    /// </summary>
    public class WoodDefectDetector : IDisposable
    {
        #region Constants
        /// <summary>
        /// Default K iperparameter value for knn classifier.
        /// </summary>
        private const int DEFAULT_K = 1;

        /// <summary>
        /// Haralick features used during image processing
        /// </summary>
        private const int DEFAULT_HARALICK_FEATURES = 13;
        
        /// <summary>
        /// Total number of features, consisting in Haralick features (one set for subimage) plus width and height
        /// </summary>
        private const int DEFAULT_NUM_FEATURES = DEFAULT_HARALICK_FEATURES * 3 + 2;

        /// <summary>
        /// Total number of normalized features
        /// </summary>
        private const int DEFAULT_NUM_NORMALIZE_FEATURES = DEFAULT_HARALICK_FEATURES * 3;

        /// <summary>
        /// The default threshold for distribution validation.
        /// </summary>
        private const double DEFAULT_VALIDATE_DISTRIBUTION_THRESHOLD = 1.97;

        /// <summary>
        /// The threshold that represents the number of data to process below which the parallelism is automatically deativated. 
        /// </summary>
        private const int PARALLISM_SUPPRESSION_THRESHOLD = 10;

        private const string DEFAULT_RESULT_DIRECTORY = "results";
        private const string DEFAULT_RESULT_TAG = "training";
        private const string RESULT_FILENAME_SUFFIX = "_wdd_results.json";
        private const string RESULT_EXTRA_FILENAME_SUFFIX = "_wdd_results_extra.json";
        #endregion

        #region Fields
        /// <summary>
        /// The K-Nearest Neighbour classifier.
        /// </summary>
        private KNearest knn;
        /// <summary>
        /// The values of the latest training.
        /// </summary>
        private TrainingValues trainingValues;

        /// <summary>
        /// K iperparameter value for the KNN classifier
        /// </summary>
        public int K { get; set; }

        /// <summary>
        /// Validation threshold. It is used to determine if the feature vector is too different from the mean feature set
        /// </summary>
        public double ValidateDistributionThreshold { get; set; }

        /// <summary>
        /// True if the detector must check for existing data result for its training
        /// </summary>
        public bool ShouldReadPreviousResults { get; set; }

        /// <summary>
        /// True if the detector must save the results of its training
        /// </summary>
        public bool ShouldSaveResults { get; set; }

        /// <summary>
        /// The result directory.
        /// </summary>
        public string ResultsDir { get; set; } = DEFAULT_RESULT_DIRECTORY;

        /// <summary>
        /// The tag of the results
        /// </summary>
        public string ResultTag { get; set; } = DEFAULT_RESULT_TAG;

        /// <summary>
        /// The degree of parallelism.
        /// </summary>
        public ParallelismDegrees DegreeOfParallelism { get; set; } = ParallelismDegrees.None;
        #endregion

        #region Events
        /// <summary>
        /// Training progress event, signals that an image have been processed.
        /// </summary>
        public event DetectorProgressHandler TrainingProgress;
        /// <summary>
        /// Test progress event, signals that an image have been processed.
        /// </summary>
        public event DetectorProgressHandler TestProgress;
        /// <summary>
        /// Signals the training completion.
        /// </summary>
        public event DetectorCompletedHandler TrainingCompleted;
        /// <summary>
        /// Signals the test completion.
        /// </summary>
        public event DetectorCompletedHandler TestCompleted;

        /// <summary>
        /// Progress event handler.
        /// </summary>
        /// <param name="sender">The detector</param>
        /// <param name="e">The event arguments</param>
        public delegate void DetectorProgressHandler(object sender, DetectorProgressEventArgs e);

        /// <summary>
        /// Completion event handler.
        /// </summary>
        /// <param name="sender">The detector</param>
        /// <param name="e">The event arguments</param>
        public delegate void DetectorCompletedHandler(object sender, DetectorCompletedEventArgs e);

        /// <summary>
        /// Signals the training progress.
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected virtual void OnTrainingProgress(DetectorProgressEventArgs e)
        {
            TrainingProgress?.Invoke(this, e);
        }

        /// <summary>
        /// Signals the test progress.
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected virtual void OnTestProgress(DetectorProgressEventArgs e)
        {
            TestProgress?.Invoke(this, e);
        }

        /// <summary>
        /// Signals the training completion.
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected virtual void OnTrainingCompleted(DetectorCompletedEventArgs e)
        {
            TrainingCompleted?.Invoke(this, e);
        }

        /// <summary>
        /// Signals the test completion.
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected virtual void OnTestCompleted(DetectorCompletedEventArgs e)
        {
            TestCompleted?.Invoke(this, e);
        }
#endregion

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="numDefectClasses">The number of defect classes.</param>
        public WoodDefectDetector()
        {
            K = DEFAULT_K;
            ValidateDistributionThreshold = DEFAULT_VALIDATE_DISTRIBUTION_THRESHOLD;
            ShouldReadPreviousResults = true;
            ShouldSaveResults = true;
            trainingValues = new TrainingValues()
            {
                NumFeatures = DEFAULT_NUM_FEATURES
            };
        }
        #endregion

        #region Getters
        private string GetSaveResultFilename()
        {
            // creates directory if missing
            Directory.CreateDirectory(ResultsDir);
            return Path.Combine(ResultsDir, ResultTag + RESULT_FILENAME_SUFFIX);
        }

        private string GetSaveExtraResultFilename()
        {
            // creates directory if missing
            Directory.CreateDirectory(ResultsDir);
            return Path.Combine(ResultsDir, ResultTag + RESULT_EXTRA_FILENAME_SUFFIX);
        }
        #endregion

        #region Main Methods
        /// <summary>
        /// Resets the classifier and the training values.
        /// </summary>
        public void Reset()
        {
            if (knn != null)
            {
                knn.Dispose();
            }

            trainingValues = new TrainingValues()
            {
                NumFeatures = DEFAULT_NUM_FEATURES
            };         
        }

        /// <summary>
        /// Executes training asynchronously.
        /// </summary>
        /// <param name="images">The images to use for training</param>
        /// <param name="trainingLabels">The labels related to the training images</param>
        /// <returns>True, if the train is successful; otherwise false, if it was skipped due to already present training results.</returns>
        public Task<bool> TrainAsync(Image<Gray, byte>[] images, int[] trainingLabels)
        {
            return Task.Run(() => Train(images, trainingLabels));
        }

        /// <summary>
        /// Executes training.
        /// </summary>
        /// <param name="images">The images to use for training</param>
        /// <param name="trainingLabels">The labels related to the training images</param>
        /// <returns>True, if the train is successful; otherwise false, if it was skipped due to already present training results.</returns>
        public bool Train(Image<Gray, byte>[] images, int[] trainingLabels)
        {
            // Setup the K-NN classifier
            SetupClassifier();

            if (ShouldReadPreviousResults ? CheckExistingResultExistance() : false)
            {
                LoadExistingResults();

                OnTrainingCompleted(new DetectorCompletedEventArgs(0L));

                return false;
            }
            else
            {
                var timer = Stopwatch.StartNew();
                timer.Start();

                // Setup the data vars
                int labelsNum = trainingLabels.Count();
                var labels = new Matrix<int>(labelsNum, 1);

                Parallel.For(0, labelsNum, i => labels[i, 0] = trainingLabels[i]);

                // Image processing and feature extraction
                var featureVector = ExtractFeatures(images);

                // Normalization
                ComputeNormalizationFactors(featureVector);
                NormalizeData(featureVector, DEFAULT_NUM_NORMALIZE_FEATURES);

                // Feature save in TrainingValues
                SaveTrainingValues(featureVector, labels);

                knn.Train(new TrainData(featureVector.Convert<float>(), DataLayoutType.RowSample, labels));

                timer.Stop();
                OnTrainingCompleted(new DetectorCompletedEventArgs(timer.ElapsedMilliseconds));

                if (ShouldSaveResults)
                {
                    //Saving the results
                    knn.Save(GetSaveResultFilename());
                    //Saving extra results info
                    SaveExtraInfo(GetSaveExtraResultFilename());
                }

                return true;
            }
        }

        /// <summary>
        /// Predict and test the result asynchronously.
        /// </summary>
        /// <param name="images">The images</param>
        /// <returns>The list of results</returns>
        public Task<IEnumerable<TestResult>> TestAsync(params Image<Gray, byte>[] images)
        {
            return Task.Run(() => Test(images));
        }

        /// <summary>
        /// Predict and test the result.
        /// </summary>
        /// <param name="images">The images</param>
        /// <returns>The list of results</returns>
        public IEnumerable<TestResult> Test(params Image<Gray, byte>[] images)
        {
            var timer = Stopwatch.StartNew();
            timer.Start();

            // Feature extraction and normalization
            var features = ExtractFeatures(images, true);
            NormalizeData(features, DEFAULT_NUM_NORMALIZE_FEATURES);

            //predict and validate
            var results = PredictAndValidate(features);

            timer.Stop();
            OnTestCompleted(new DetectorCompletedEventArgs(timer.ElapsedMilliseconds));

            return results;
        }
        #endregion

        #region Processing Methods
        /// <summary>
        /// Extracts features.
        /// </summary>
        /// <param name="images">Training images</param>
        /// <returns>The matrix in which the rows represents a single feature vector</returns>
        private Matrix<double> ExtractFeatures(Image<Gray, byte>[] images, bool isTest = false)
        {
            var size = images.Count();
            var result = new Matrix<double>(size, DEFAULT_NUM_FEATURES);

            // Retrieve interesting features
            switch (DegreeOfParallelism) {
                default:
                case ParallelismDegrees.None:
                case ParallelismDegrees.Hybrid:                    
                    {
                        for (int i = 0; i < size; i++)
                        {
                            var features = ExtractFeatures(images[i], isTest);
                            for (int j = 0; j < features.Length; j++)
                            {
                                result[i, j] = features[j];
                            }
                        }
                        break;
                    }
                case ParallelismDegrees.Medium:
                    {
                        Parallel.For(0, size, new ParallelOptions()
                        {
                            MaxDegreeOfParallelism = Environment.ProcessorCount <= 2 ? 1 : Environment.ProcessorCount / 2
                        }, i =>
                        {
                            var features = ExtractFeatures(images[i], isTest);
                            for (int j = 0; j < features.Length; j++)
                            {
                                result[i, j] = features[j];
                            }
                        });
                        break;
                    }
                case ParallelismDegrees.High:
                    {
                        Parallel.For(0, size, new ParallelOptions()
                        {
                            MaxDegreeOfParallelism = Environment.ProcessorCount <= 3 ? 1 : Environment.ProcessorCount / 3
                        }, i =>
                        {
                            var features = ExtractFeatures(images[i], isTest);
                            for (int j = 0; j < features.Length; j++)
                            {
                                result[i, j] = features[j];
                            }
                        });
                        break;
                    }
                case ParallelismDegrees.Low:
                    {
                        Parallel.For(0, size, i =>
                        {
                            var features = ExtractFeatures(images[i], isTest);
                            for (int j = 0; j < features.Length; j++)
                            {
                                result[i, j] = features[j];
                            }
                        });
                        break;
                    }
            }

            return result;
        }

        /// <summary>
        /// Extracts features from a single images.
        /// </summary>
        /// <param name="image">The image</param>
        /// <param name="isTest">Whether this computation is done during testing or not</param>
        /// <returns>The feature vector</returns>
        private double[] ExtractFeatures(Image<Gray, byte> image, bool isTest = false)
        {
            var timer = Stopwatch.StartNew();
            timer.Start();
            var featureVector = ExtractFeatureVector(image);
            timer.Stop();

            if (isTest)
            {
                OnTestProgress(new DetectorProgressEventArgs(timer.ElapsedMilliseconds));
            }
            else
            {
                OnTrainingProgress(new DetectorProgressEventArgs(timer.ElapsedMilliseconds));
            }
    
            return featureVector;
        }

        /// <summary>
        /// Calculates a vector of interesting features (blocking method by design).
        /// </summary>
        /// <param name="image">The training image</param>
        /// <returns>The feature vector</returns>
        private double[] ExtractFeatureVector(Image<Gray, byte> image)
        {
            // Creating the feature vector
            var featureVector = new double[DEFAULT_NUM_FEATURES];

            switch (DegreeOfParallelism)
            {
                default:
                case ParallelismDegrees.None:
                case ParallelismDegrees.Low:
                    {                       
                        // 1) Mod gradient and compute the 4-dimensional feature vector
                        GradientUtils.ComputeGradient(image, out Image<Gray, float> xGradient, out Image<Gray, float> yGradient, out Image<Gray, float> modGradient, out double averageGradient);
                        xGradient.Dispose();
                        yGradient.Dispose();

                        using (modGradient)
                        // 2) Process image with contrast enhancement
                        using (var enhancedImage = PreprocessImage(image))
                        using (var coOccurrenceMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(enhancedImage))
                        using (var modGray = modGradient.Convert<Gray, byte>())
                        using (var coOccurrenceModGradientMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(modGray))
                        // 3) Compute the edge detection result image and compute the 4-dimensional feature vector
                        using (var edgeDetection = modGray.Canny(150, 50))
                        using (var coOccurrenceEdgeDetectionMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(edgeDetection))
                        {
                            var enhancedImageFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceMatrix);
                            var gradientImageFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceModGradientMatrix);
                            var edgeDetectionFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceEdgeDetectionMatrix);

                            for (int i = 0; i < DEFAULT_HARALICK_FEATURES; i++)
                            {
                                featureVector[i] = enhancedImageFeatureVector[i];
                                featureVector[i + DEFAULT_HARALICK_FEATURES] = gradientImageFeatureVector[i];
                                featureVector[i + DEFAULT_HARALICK_FEATURES * 2] = edgeDetectionFeatureVector[i];
                            }

                            featureVector[DEFAULT_HARALICK_FEATURES * 3] = image.Width;
                            featureVector[DEFAULT_HARALICK_FEATURES * 3 + 1] = image.Height;
                        }
                        
                        break;
                    }
                case ParallelismDegrees.Medium:
                case ParallelismDegrees.Hybrid:
                    {
                        GradientUtils.ComputeGradient(image, out Image<Gray, float> xGradient, out Image<Gray, float> yGradient, out Image<Gray, float> modGradient, out double averageGradient);
                        xGradient.Dispose();
                        yGradient.Dispose();

                        using (modGradient)
                        using (var modGray = modGradient.Convert<Gray, byte>())
                        {

                            // 1) Compute the image size
                            featureVector[DEFAULT_HARALICK_FEATURES * 3] = image.Width;
                            featureVector[DEFAULT_HARALICK_FEATURES * 3 + 1] = image.Height;

                            Task.WhenAll(
                                // 2) Computation of the 4-dimensional feature vector from the unoriented co-occurrence matrix
                                Task.Run(() =>
                                {
                                    using (var enhancedImage = PreprocessImage(image))
                                    using (var coOccurrenceMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(enhancedImage))
                                    {
                                        var enhancedImageFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceMatrix);
                                        for (int i = 0; i < DEFAULT_HARALICK_FEATURES; i++)
                                        {
                                            featureVector[i] = enhancedImageFeatureVector[i];
                                        }
                                    }
                                }),
                                // 3) Compute the mod gradient and compute the 4-dimensional feature vector
                                Task.Run(() =>
                                {
                                    using (var coOccurrenceModGradientMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(modGray))
                                    {
                                        var gradientImageFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceModGradientMatrix);
                                        for (int i = 0; i < DEFAULT_HARALICK_FEATURES; i++)
                                        {
                                            featureVector[i + DEFAULT_HARALICK_FEATURES] = gradientImageFeatureVector[i];
                                        }
                                    }
                                }),
                                // 4) Compute the edge detection result image and compute the 4-dimensional feature vector
                                Task.Run(() =>
                                {
                                    using(var edgeDetection = modGray.Canny(150, 50))
                                    using (var coOccurrenceEdgeDetectionMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(edgeDetection))
                                    {
                                        var edgeDetectionFeatureVector = GenerateFeaturesFromMatrix(coOccurrenceEdgeDetectionMatrix);
                                        for (int i = 0; i < DEFAULT_HARALICK_FEATURES; i++)
                                        {
                                            featureVector[i + DEFAULT_HARALICK_FEATURES * 2] = edgeDetectionFeatureVector[i];
                                        }
                                    }
                                })
                            ).Wait();
                        }

                        break;
                    }
                case ParallelismDegrees.High:               
                    {
                        // Setup: pre-calculate all data
                        GradientUtils.ComputeGradient(image, out Image<Gray, float> xGradient, out Image<Gray, float> yGradient, out Image<Gray, float> modGradient, out double averageGradient2);
                        xGradient.Dispose();
                        yGradient.Dispose();

                        using (modGradient)
                        using (var enhancedImage = PreprocessImage(image))
                        using (var modGray = modGradient.Convert<Gray, byte>())
                        using (var edgeDetection = modGray.Canny(150, 50))
                        using (var coOccurrenceMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(enhancedImage))
                        using (var coOccurrenceModGradientMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(modGray))
                        using (var coOccurrenceEdgeDetectionMatrix = CoOccurrenceMatrixFeatures.ComputeUnorientedCoOccurrenceMatrix(edgeDetection))
                        {
                            // 1) Compute the image size
                            featureVector[DEFAULT_HARALICK_FEATURES * 3] = image.Width;
                            featureVector[DEFAULT_HARALICK_FEATURES * 3 + 1] = image.Height;

                            Task.WhenAll(
                                // 2) Computation of the 4-dimensional feature vector from the unoriented co-occurrence matrix
                                Task.Run(() => featureVector[0] = CoOccurrenceMatrixFeatures.ComputeAngularSecondMoment(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[1] = CoOccurrenceMatrixFeatures.ComputeContrast(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[2] = CoOccurrenceMatrixFeatures.ComputeCorrelationCoefficient(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[3] = CoOccurrenceMatrixFeatures.ComputeSumsOfSquares(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[4] = CoOccurrenceMatrixFeatures.ComputeInverseDifferenceMoment(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[5] = CoOccurrenceMatrixFeatures.ComputeSumAverage(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[6] = CoOccurrenceMatrixFeatures.ComputeSumVariance(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[7] = CoOccurrenceMatrixFeatures.ComputeSumEntropy(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[8] = CoOccurrenceMatrixFeatures.ComputeEntropy(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[9] = CoOccurrenceMatrixFeatures.ComputeDifferenceVariance(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[10] = CoOccurrenceMatrixFeatures.ComputeDifferenceEntropy(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[11] = CoOccurrenceMatrixFeatures.ComputeHomogeneity(coOccurrenceMatrix)),
                                Task.Run(() => featureVector[12] = CoOccurrenceMatrixFeatures.ComputeEnergy(coOccurrenceMatrix)),
                                // 3) Compute the mod gradient and compute the 4-dimensional feature vector
                                Task.Run(() => featureVector[0 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeAngularSecondMoment(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[1 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeContrast(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[2 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeCorrelationCoefficient(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[3 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeSumsOfSquares(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[4 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeInverseDifferenceMoment(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[5 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeSumAverage(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[6 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeSumVariance(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[7 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeSumEntropy(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[8 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeEntropy(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[9 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeDifferenceVariance(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[10 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeDifferenceEntropy(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[11 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeHomogeneity(coOccurrenceModGradientMatrix)),
                                Task.Run(() => featureVector[12 + DEFAULT_HARALICK_FEATURES] = CoOccurrenceMatrixFeatures.ComputeEnergy(coOccurrenceModGradientMatrix)),
                                // 4) Compute the edge detection result image and compute the 4-dimensional feature vector
                                Task.Run(() => featureVector[0 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeAngularSecondMoment(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[1 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeContrast(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[2 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeCorrelationCoefficient(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[3 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeSumsOfSquares(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[4 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeInverseDifferenceMoment(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[5 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeSumAverage(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[6 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeSumVariance(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[7 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeSumEntropy(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[8 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeEntropy(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[9 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeDifferenceVariance(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[10 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeDifferenceEntropy(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[11 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeHomogeneity(coOccurrenceEdgeDetectionMatrix)),
                                Task.Run(() => featureVector[12 + DEFAULT_HARALICK_FEATURES * 2] = CoOccurrenceMatrixFeatures.ComputeEnergy(coOccurrenceEdgeDetectionMatrix))
                            ).Wait();
                        }

                        break;
                    }
            }

            return featureVector;
        }

        /// <summary>
        /// Generates Haralick's features from a co-occurence matrix.
        /// </summary>
        /// <param name="coOccurrenceMatrix">The co-occurrence matrix</param>
        /// <returns>The feature vector</returns>
        private double[] GenerateFeaturesFromMatrix(Matrix<double> coOccurrenceMatrix)
        {
            var features = new double[DEFAULT_HARALICK_FEATURES];

            // used by correlation coefficient and sums of squares
            var µ = CoOccurrenceMatrixFeatures.Mean(coOccurrenceMatrix, out var µx, out var µy);

            var angularSecondMoment = CoOccurrenceMatrixFeatures.ComputeAngularSecondMoment(coOccurrenceMatrix);
            features[0] = angularSecondMoment;
            features[1] = CoOccurrenceMatrixFeatures.ComputeContrast(coOccurrenceMatrix);
            features[2] = CoOccurrenceMatrixFeatures.ComputeCorrelationCoefficient(coOccurrenceMatrix, µx, µy);
            features[3] = CoOccurrenceMatrixFeatures.ComputeSumsOfSquares(coOccurrenceMatrix, µ);
            features[4] = CoOccurrenceMatrixFeatures.ComputeInverseDifferenceMoment(coOccurrenceMatrix);
            features[5] = CoOccurrenceMatrixFeatures.ComputeSumAverage(coOccurrenceMatrix);

            var sumEntropy = CoOccurrenceMatrixFeatures.ComputeSumEntropy(coOccurrenceMatrix);
            features[6] = CoOccurrenceMatrixFeatures.ComputeSumVariance(coOccurrenceMatrix, sumEntropy);
            features[7] = sumEntropy;

            var entropy = CoOccurrenceMatrixFeatures.ComputeEntropy(coOccurrenceMatrix);
            features[8] = entropy;
            features[9] = CoOccurrenceMatrixFeatures.ComputeDifferenceVariance(coOccurrenceMatrix);
            features[10] = CoOccurrenceMatrixFeatures.ComputeDifferenceEntropy(coOccurrenceMatrix);

            // This are not Haralick features
            features[11] = CoOccurrenceMatrixFeatures.ComputeHomogeneity(coOccurrenceMatrix);
            features[12] = CoOccurrenceMatrixFeatures.ComputeEnergy(coOccurrenceMatrix, angularSecondMoment);

            return features;
        }

        /// <summary>
        /// Preprocess the image.
        /// </summary>
        /// <param name="image">The image</param>
        /// <returns>The preprocessed image</returns>
        private Image<Gray, byte> PreprocessImage(Image<Gray, byte> image)
        {
            // Enhance contrast
            var smooth = image.SmoothGaussian(5);
            var smoothContrastEnhanced = ImageProcessingUtils.ContrastEhnancement(smooth);
            return smoothContrastEnhanced;
        }

        /// <summary>
        /// Feature-wise computation of the minimum and maximum values for feature normalization
        /// </summary>
        /// <param name="featureData">The feature vectors matrix</param>
        private void ComputeNormalizationFactors(Matrix<double> featureData)
        {
            // Feature-wise computation of the minimum and maximum values for feature normalization
            var minVals = new double[featureData.Cols];
            var maxVals = new double[featureData.Cols];

            Parallel.For(0, featureData.Rows, r =>
            {
                for (int c = 0; c < featureData.Cols; c++)
                {
                    minVals[c] = Math.Min(minVals[c], featureData[r, c]);
                    maxVals[c] = Math.Max(maxVals[c], featureData[r, c]);
                }         
            });

            // Saving the current values
            trainingValues.MinVals = minVals;
            trainingValues.MaxVals = maxVals;
        }

        /// <summary>
        /// Normalize feature vectors according to the range of values observed during training
        /// </summary>
        /// <param name="featureData">The feature vectors matrix</param>
        /// <param name="numberOfFeature">The number of features</param>
        private void NormalizeData(Matrix<double> featureData, int numberOfFeature)
        {
            var minVals = trainingValues.MinVals;
            var maxVals = trainingValues.MaxVals;

            // Normalize feature vectors according to the range of values observed during training
            var ranges = new double[minVals.Length];

            if (numberOfFeature > ranges.Length)
            {
                throw new ArgumentException("Unable to normalize feature vector, its length is greater than the training values count");
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                ranges[i] = maxVals[i] - minVals[i];
            }

            Parallel.For(0, featureData.Rows, r =>
            {
                for (int c = 0; c < numberOfFeature; c++)
                {
                    // If the range is equal 0 it means that every feature is 0
                    featureData[r, c] = (ranges[c] == 0d) ? 0d : ((featureData[r, c] - minVals[c]) / ranges[c]);
                }
            });
        }

        /// <summary>
        /// Predicts and validates the class of every feature vector in the input matrix.
        /// </summary>
        /// <param name="featureData">The feature vectors matrix</param>
        /// <returns>The list of predictions</returns>
        private IEnumerable<TestResult> PredictAndValidate(Matrix<double> featureData)
        {
            var size = featureData.Rows;
            var results = new TestResult[size];

            switch (DegreeOfParallelism)
            {
                default:
                case ParallelismDegrees.None:
                case ParallelismDegrees.Hybrid:
                    {
                        for (int i = 0; i < size; i++)
                        {
                            var featureVector = featureData.GetRow(i);
                            var prediction = (int)knn.Predict(featureVector.Convert<float>());
                            results[i] = ValidatePrediction(featureVector, prediction);
                        }
                        break;
                    }           
                case ParallelismDegrees.Low:
                case ParallelismDegrees.Medium:
                case ParallelismDegrees.High:
                    {
                        Parallel.For(0, size, i =>
                        {
                            var featureVector = featureData.GetRow(i);
                            var prediction = (int)knn.Predict(featureVector.Convert<float>());
                            results[i] = ValidatePrediction(featureVector, prediction);
                        });
                        break;
                    }
            }

            return results;
        }

        /// <summary>
        /// Validates a prediction.
        /// </summary>
        /// <param name="featureVector">The feature vectors matricx</param>
        /// <param name="prediction">The prediction</param>
        /// <returns>The result of the validated prediction</returns>
        private TestResult ValidatePrediction(Matrix<double> featureVector, int prediction)
        {
            var varianceThreshold = new bool[DEFAULT_NUM_FEATURES];
            var sampleVarianceThreshold = new bool[DEFAULT_NUM_FEATURES];

            // Retrieve true class mean, variance and sample variance feature sets
            var trueMeanFeatureSet = trainingValues.GetMeanFeatureSetAsMatrix(prediction);
            var trueVarianceFeatureSet = trainingValues.GetVarianceFeatureSetAsMatrix(prediction);
            var trueSampleVarianceFeatureSet = trainingValues.GetSampleVarianceFeatureSetAsMatrix(prediction);

            switch (DegreeOfParallelism)
            {
                default:
                case ParallelismDegrees.None:
                case ParallelismDegrees.Low:
                    {
                        for (int i = 0; i < DEFAULT_NUM_FEATURES; i++)
                        {
                            var val = featureVector[0, i];

                            var negNormal = trueMeanFeatureSet[0, i] - ValidateDistributionThreshold * Math.Sqrt(trueVarianceFeatureSet[0, i]);
                            var posNormal = trueMeanFeatureSet[0, i] + ValidateDistributionThreshold * Math.Sqrt(trueVarianceFeatureSet[0, i]);

                            // Too far in the distribution
                            varianceThreshold[i] = !(val < negNormal || val > posNormal);

                            var negSample = trueMeanFeatureSet[0, i] - ValidateDistributionThreshold * Math.Sqrt(trueSampleVarianceFeatureSet[0, i]);
                            var posSample = trueMeanFeatureSet[0, i] + ValidateDistributionThreshold * Math.Sqrt(trueSampleVarianceFeatureSet[0, i]);

                            // Too far in the distribution
                            sampleVarianceThreshold[i] = !(val < negSample || val > posSample);
                        }

                        break;
                    }           
                case ParallelismDegrees.Hybrid:
                case ParallelismDegrees.Medium:
                case ParallelismDegrees.High:         
                    {
                        Parallel.For(0, DEFAULT_NUM_FEATURES, i =>
                        {
                            var val = featureVector[0, i];

                            var negNormal = trueMeanFeatureSet[0, i] - ValidateDistributionThreshold * Math.Sqrt(trueVarianceFeatureSet[0, i]);
                            var posNormal = trueMeanFeatureSet[0, i] + ValidateDistributionThreshold * Math.Sqrt(trueVarianceFeatureSet[0, i]);

                            // Too far in the distribution
                            varianceThreshold[i] = !(val < negNormal || val > posNormal);

                            var negSample = trueMeanFeatureSet[0, i] - ValidateDistributionThreshold * Math.Sqrt(trueSampleVarianceFeatureSet[0, i]);
                            var posSample = trueMeanFeatureSet[0, i] + ValidateDistributionThreshold * Math.Sqrt(trueSampleVarianceFeatureSet[0, i]);

                            // Too far in the distribution
                            sampleVarianceThreshold[i] = !(val < negSample || val > posSample);
                        });

                        break;
                    }
            }

            var isInsideThresholdForVariance = varianceThreshold.Any(i => i == true);
            var isInsideThresholdForSampleVariance = sampleVarianceThreshold.Any(i => i == true);

            return new TestResult(prediction, featureVector, trueMeanFeatureSet, trueVarianceFeatureSet, trueSampleVarianceFeatureSet, isInsideThresholdForVariance, isInsideThresholdForSampleVariance);
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Setups the classifier.
        /// </summary>
        private void SetupClassifier()
        {
            //clears the classifier if it already exists
            if (knn != null)
            {
                knn.Dispose();
            }

            // Setup the K-NN classifier
            knn = new KNearest
            {
                DefaultK = K,
                IsClassifier = true
            };
        }

        /// <summary>
        /// Checks results file existance.
        /// </summary>
        /// <returns>True, if previous results are found; otherwise false.</returns>
        private bool CheckExistingResultExistance()
        {
            return File.Exists(GetSaveResultFilename()) && File.Exists(GetSaveExtraResultFilename());
        }

        /// <summary>
        /// Loads existing results.
        /// </summary>
        private void LoadExistingResults()
        {
            LoadExistingResults(GetSaveResultFilename(), GetSaveExtraResultFilename());
        }

        /// <summary>
        /// Loads existing results.
        /// </summary>
        /// <param name="saveResultFileName"></param>
        /// <param name="saveExtraResultFilename"></param>
        private void LoadExistingResults(string saveResultFileName, string saveExtraResultFilename)
        {
            //Loading the previous results
            if (!File.Exists(saveResultFileName))
            {
                throw new ArgumentException("Cannot load result info for KNN classifier, the file was not found at path " + saveResultFileName);
            }
            knn.Load(saveResultFileName);
            //Load extra info about previous results
            LoadExtraInfo(saveExtraResultFilename);
        }

        /// <summary>
        /// Saves the training values.
        /// </summary>
        /// <param name="tData"></param>
        /// <param name="tLab"></param>
        private void SaveTrainingValues(Matrix<double> tData, Matrix<int> tLab)
        {
            // We save in TrainingValues what we have found for further analysis
            // do not parallelize this
            for (int i = 0; i < tData.Rows; i++)
            {
                var featureSet = new double[DEFAULT_NUM_FEATURES];
                for (int j = 0; j < DEFAULT_NUM_FEATURES; j++)
                {
                    featureSet[j] = tData[i, j];
                }
                trainingValues.AddFeatureSet(tLab[i, 0], featureSet);
            }
        }

        /// <summary>
        /// Loads extra info for previous results.
        /// </summary>
        /// <param name="fileName">The filename where extra info are stored.</param>
        private void LoadExtraInfo(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new ArgumentException("Cannot load extra result info for KNN classifier, the file was not found at path " + fileName);
            }

            using (StreamReader file = new StreamReader(fileName))
            {
                var json = JObject.Parse(File.ReadAllText(fileName));
                trainingValues.LoadJson(json);
            }

        }

        /// <summary>
        /// Saves extra info about the current results.
        /// </summary>
        /// <param name="fileName">The filename where extra info should be stored.</param>
        private void SaveExtraInfo(string fileName)
        {
            var json = trainingValues.ToJson();
            using (JsonTextWriter writer = new JsonTextWriter(File.CreateText(GetSaveExtraResultFilename())))
            {
                json.WriteTo(writer);
            }
        }
#endregion

        #region Disposable pattern
        /// <summary>
        /// Disposes the instance and all its properties.
        /// </summary>
        public void Dispose()
        {
            if (knn != null)
            {
                knn.Dispose();
            }
            knn = null;
            trainingValues = null;
        }
        #endregion
    }
}
