import glob
import pickle
import numpy as np
from PIL import Image
import shutil
import os

def overwrite_dir(dir):
    """Creates a new results directory"""
    if os.path.exists(dir):
        shutil.rmtree(dir, ignore_errors=True)
    os.makedirs(dir)

if __name__=='__main__':
    dir = 'extracted_data'
    overwrite_dir(dir)
    # Load filenames of images (.bmp files) and text information (.def files)
    bmp_filelist = sorted(glob.glob('*.bmp'))
    def_filelist = sorted(glob.glob('*.def'))

    # Extract images to numpy arrays
    images = []
    for fname in bmp_filelist:
        images.append(np.array(Image.open(fname),dtype=np.int16))

    # Extract corner points and labels information from .def files
    text_as_list= []
    for fname in def_filelist:
        text = np.genfromtxt(fname, dtype = np.int,skip_header=7, delimiter = ' ',usecols = (0, 1, 2, 3, 4), invalid_raise = False, encoding='utf8')
        text = text.tolist()
        text_as_list.append(text)
        
    # Finally cut images with corner points. After that name files and save them to folder dir
    output_images = []
    labels = []
    for i in range(len(images)):
        img = images[i]
        for j in range(len(text_as_list[i])):
            txt = text_as_list[i][j]
            cropped_image = img[txt[1]:txt[3],txt[0]:txt[2]]
            output_images.append(cropped_image)
            labels.append(txt[4])			
       
    dataset = [output_images, labels]
    pickle.dump(dataset, open('salumdataset.pkl', 'wb'), protocol=2)
    counter = [0] * 41
    for i in range(len(labels)):
        im = Image.fromarray(output_images[i].astype('uint8'))
        counter[labels[i]-1] += 1
        if len(str(labels[i]))==1:
            name = '0' + str(labels[i])
        else:
            name = str(labels[i])
        im.save('./{}/class{}_{}.png'.format(dir, name, counter[labels[i]-1]), 'PNG')
		
    """		
    # This is how you can load .pkl dataset
    with open('salumdataset.pkl', 'rb') as f:
        traindata = pickle.load(f)
    # Images and labels
    X_train = traindata[0]
    Y_train = traindata[1]
    """


