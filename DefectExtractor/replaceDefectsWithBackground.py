import glob
import pickle
import numpy as np
from PIL import Image
import shutil
import os
import random

def overwrite_dir(dir):
    """Creates a new results directory"""
    if os.path.exists(dir):
        shutil.rmtree(dir, ignore_errors=True)
    os.makedirs(dir)

def pretty_format(defect_class_number) -> str:
    """Adds a 0 as prefix if the defect class number is less than 10"""
    string_defect_class_number = str(defect_class_number)
    if len(string_defect_class_number)==1:
        return '0' + string_defect_class_number
    else:
        return string_defect_class_number

if __name__=='__main__':
    dir = 'extracted_data'
    overwrite_dir(dir)
    # Load images (.bmp files) and text information (.def files) filenames
    bmp_filelist = sorted(glob.glob('*.bmp'))
    def_filelist = sorted(glob.glob('*.def'))
    background = np.array(Image.open("../background.bmp"),dtype=np.int16)
    width = len(background[0])
    height = len(background)

    random.seed(177013)

    for idx, fname in enumerate(bmp_filelist):
        # Extract image to numpy arrays
        img = np.array(Image.open(fname),dtype=np.int16)
        current_name = bmp_filelist[idx][:-4]

        # Extract corner points and labels information from .def files
        text = np.genfromtxt(def_filelist[idx], dtype = np.int,skip_header=7, delimiter = ' ', usecols = (0, 1, 2, 3, 4), invalid_raise = False, encoding='utf8')
        text = text.tolist()

        # For each defect replace its area with custom colour
        for i in range(len(text)):
            txt = text[i]
            for x in range(txt[1], txt[3]):
                for y in range(txt[0], txt[2]):
                    randx = random.randint(0, height-1)
                    randy = random.randint(0, width-1)
                    img[x,y] = background[randx, randy]
                    # img[x,y] = [255,0,0]
        
        im = Image.fromarray(img.astype('uint8'))
        im.save('./{}/{}_filled.bmp'.format(dir, current_name))
