import glob
import pickle
import numpy as np
from PIL import Image
import shutil
import os

def overwrite_dir(dir):
    """Creates a new results directory"""
    if os.path.exists(dir):
        shutil.rmtree(dir, ignore_errors=True)
    os.makedirs(dir)

def pretty_format(defect_class_number) -> str:
    """Adds a 0 as prefix if the defect class number is less than 10"""
    string_defect_class_number = str(defect_class_number)
    if len(string_defect_class_number)==1:
        return '0' + string_defect_class_number
    else:
        return string_defect_class_number

if __name__=='__main__':
    dir = 'extracted_data'
    overwrite_dir(dir)
    # Load images (.bmp files) and text information (.def files) filenames
    bmp_filelist = sorted(glob.glob('*.bmp'))
    def_filelist = sorted(glob.glob('*.def'))

    # Stores the number of defects found for each class (41 defects classes)
    counter = [0] * 41

    for idx, fname in enumerate(bmp_filelist):
        # Extract image to numpy arrays
        img = np.array(Image.open(fname),dtype=np.int16)
        current_name = bmp_filelist[idx][:-4]
        #print(current_name)

        # Extract corner points and labels information from .def files
        text = np.genfromtxt(def_filelist[idx], dtype = np.int,skip_header=7, delimiter = ' ',usecols = (0, 1, 2, 3, 4), invalid_raise = False, encoding='utf8')
        text = text.tolist()

        # Finally cut image with corner points. After that name files and save them to folder dir
        output_images = []
        labels = []
        image_defects_counter = {}
        for i in range(len(text)):
            txt = text[i]
            cropped_image = img[txt[1]:txt[3],txt[0]:txt[2]]
            #print(cropped_image)
            output_images.append(cropped_image)
            labels.append(txt[4])

        dataset = [output_images, labels]
        pickle.dump(dataset, open('salumdataset.pkl', 'wb'), protocol=2)
        for i in range(len(labels)):
            im = Image.fromarray(output_images[i].astype('uint8'))
            counter[labels[i]-1] += 1

            defect_class_number = pretty_format(labels[i])
            
            #im.save('./{}/class{}_{}.png'.format(dir, name, counter[labels[i]-1]), 'PNG')
            #im.save('./{}/{}_class{}_{}.png'.format(dir, current_name, defect_class_number, counter[labels[i]-1]), 'PNG')
            im.save('./{}/{}_class{}_{}.bmp'.format(dir, current_name, defect_class_number, counter[labels[i]-1]))

    """		
    # This is how you can load .pkl dataset
    with open('salumdataset.pkl', 'rb') as f:
        traindata = pickle.load(f)
    # Images and labels
    X_train = traindata[0]
    Y_train = traindata[1]
    """


