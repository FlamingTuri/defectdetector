﻿using DefectDetector;
using Emgu.CV.Structure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var detector = new WoodDefectDetector();
            detector.SetupKnnClassifier();
            detector.LoadExistingResults();

            var files = GetFilesByExtensions(new DirectoryInfo(Constants.TestDataDirName), ".bmp", ".png", ".jpg");
            int wrongPredictions = 0;
            var dictionary = new ConcurrentDictionary<string, Tuple<int, int>>();
            Parallel.ForEach(files, async file =>
            {
                string filename = file.FullName;
                var image = new Emgu.CV.Image<Gray, byte>(filename);
                var defectClass = GetDefectClassFromFileName(filename);
                var prediction = await detector.Test(image);

                if (defectClass != prediction.Prediction)
                {
                    dictionary.TryAdd(file.Name, new Tuple<int, int>(defectClass, prediction.Prediction));
                    wrongPredictions++;
                }
            });

            foreach (var entry in dictionary)
            {
                var filename = entry.Key;
                var defectClass = entry.Value.Item1;
                var prediction = entry.Value.Item2;
                Console.WriteLine("Prediction of selection " + filename + " (should be " + defectClass + "): " + prediction);
            }

            double errorRate = (double)wrongPredictions / files.Count() * 100;
            Console.WriteLine("Error rate: " + errorRate + " %");

            Console.ReadKey();
        }

        public static IEnumerable<FileInfo> GetFilesByExtensions(DirectoryInfo dir, params string[] extensions)
        {
            return dir.EnumerateFiles().Where(f => extensions.Contains(f.Extension));
        }

        public static int GetDefectClassFromFileName(string filename)
        {
            //[MG: ahah]
            //Console.WriteLine(filename);
            var s1 = Regex.Split(filename, "class");
            var s2 = Regex.Split(s1[1], "_");
            return Convert.ToInt32(s2[0]);
        }
    }
}
