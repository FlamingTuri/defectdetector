﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace DefectDetectorCLI.Utils
{
    /// <summary>
    /// The image set utilities
    /// </summary>
    public class ImageSetUtils
    {
        private static readonly string[] Extensions = { ".bmp", ".png", ".jpg" };

        /// <summary>
        /// Given a directory, analyzes the files matching with the specified extensions and gets the defect count for each class
        /// </summary>
        /// <param name="directory">The directory to analyze</param>
        /// <param name="images">The matching files</param>
        /// <param name="defectsCountDictionary">Mapping [defect class -> count]</param>
        public static void GetDefectsCountForEachClass(string directory, out List<FileInfo> images, out Dictionary<int, long> defectsCountDictionary)
        {
            images = new List<FileInfo>();
            defectsCountDictionary = new Dictionary<int, long>();
            foreach (FileInfo file in FileUtils.GetFilesByExtensions(new DirectoryInfo(directory), Extensions))
            {
                images.Add(file);
                int defectClass = GetDefectClassFromFileName(file.Name);
                if (defectsCountDictionary.ContainsKey(defectClass))
                {
                    defectsCountDictionary[defectClass] += 1;
                }
                else
                {
                    defectsCountDictionary.Add(defectClass, 1);
                }
            }
        }

        /// <summary>
        /// Extracts the defect class from the file name. The file should be named as *classX_*, where * can be any value and X is the class number.
        /// </summary>
        /// <param name="filename">The file name</param>
        /// <returns>The number of the defect class</returns>
        public static int GetDefectClassFromFileName(string filename)
        {
            var match = new Regex(@"_class(\d+)_").Match(filename);
            if (match.Success && int.TryParse(match.Groups[1].Value, out int value))
            {
                return value;            
            }

            throw new ArgumentException("Cannot parse the defect class from file with name: " + filename);
        }
    }
}
