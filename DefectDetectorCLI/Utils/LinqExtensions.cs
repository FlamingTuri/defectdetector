﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DefectDetector.CLI.Utils
{
    /// <summary>
    /// The enumerable extension methods
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Splits a collection into parts.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the collection</typeparam>
        /// <param name="list">The list to split</param>
        /// <param name="parts">The number of parts to create</param>
        /// <returns>The a list that contains n collections, where n is the number of parts.</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list, int parts)
        {
            return list.Select((item, index) => new { index, item })
                       .GroupBy(x => x.index % parts)
                       .Select(x => x.Select(y => y.item));
        }

        /// <summary>
        /// Genegerates different combinations of the elements of the enumerable.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the enumerable</typeparam>
        /// <param name="elements">The enumerable</param>
        /// <param name="k">The number od different combinations to generate</param>
        /// <returns>The enumerable of combinations</returns>
        public static IEnumerable<IEnumerable<T>> DifferentCombinations<T>(this IEnumerable<T> elements, int k)
        {
            return k == 0 ? new[] { new T[0] } :
              elements.SelectMany((e, i) =>
                elements.Skip(i + 1).DifferentCombinations(k - 1).Select(c => (new[] { e }).Concat(c)));
        }

        /// <summary>
        /// Shuffles the elements in the enumerable
        /// </summary>
        /// <typeparam name="T">The type of the elements of the enumerable</typeparam>
        /// <param name="elements">The enumerable</param>
        /// <returns>The shuffled enumerable</returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> elements)
        {
            var list = new List<T>(elements);
            var random = new Random();
            int n = list.Count();
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }
    }
}
