using CommandLine;
using CommandLine.Text;
using DefectDetector;
using DefectDetector.CLI.Utils;
using DefectDetector.Models;
using DefectDetectorCLI.Utils;
using Emgu.CV;
using Emgu.CV.Structure;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;

namespace DefectDetectorCLI
{
    class Program
    {
        private static readonly object consoleMutex = new object();
        private static Logger Log;

        //define arguments
        class Options
        {
            [Value(0, Required = true, HelpText = "The input path that contains the images to process")]
            public string InputPath { get; set; }

            [Option('p', "parallelism", Default = ParallelismDegrees.None, Required = false, HelpText = "Set the degree of parallelism (None, Hybrid, Low, Medium, High)")]
            public ParallelismDegrees Parallelism { get; set; }

            [Option('k', "validation-folds", Default = 4, Required = false, HelpText = "Set the K value for K-Fold Cross-Validation")]
            public int KFolds { get; set; }

            [Option('l', "log-level", Default = LogEventLevel.Information, Required = false, HelpText = "Set the log level (Verbose, Debug, Information, Warning, Error, Fatal)")]
            public LogEventLevel LogLevel { get; set; }

            [Option('f', "log-file", Required = false, HelpText = "Set the log file path (enables the file logging if the path is valid)")]
            public string LogFile { get; set; }

            [Option('w', "wait-user-command", Default = false, Required = false, HelpText = "Set whether the user should confirm to continue with the next K-Fold Cross-Validation iteration")]
            public bool WaitUserCommand { get; set; }

            [Option('s', "save-train-data", Default = false, Required = false, HelpText = "Set whether the training data should be saved")]
            public bool SaveTrainData { get; set; }

            [Option('t', "train-data-tag", Default = "training", Required = false, HelpText = "Set the training data tag, the generated file will have the following name: \"{TAG}_{FOLD_NUM}\" (Default: training)")]
            public string TrainDataTag { get; set; }

            [Usage]
            public static IEnumerable<Example> Examples
            {
                get
                {
                    return new List<Example>()
                    {
                        new Example("Normal scenario", UnParserSettings.WithGroupSwitchesOnly(), new Options { InputPath = "C:\\test_data" }),
                        new Example("High performance", UnParserSettings.WithGroupSwitchesOnly(), new Options { InputPath = "C:\\test_data", Parallelism = ParallelismDegrees.High }),
                        new Example("Verbose logging", UnParserSettings.WithGroupSwitchesOnly(), new Options { InputPath = "C:\\test_data", LogLevel = LogEventLevel.Debug }),
                        new Example("Save train data", UnParserSettings.WithGroupSwitchesOnly(), new Options { InputPath = "C:\\test_data", SaveTrainData = true, TrainDataTag = "my_tag" })
                    };
                }
            }
        }

        public static void Main(string[] args)
        {
            try
            {
                //synchronization mutex to assure that all tasks have been completed
                var mutexTrain = new ManualResetEvent(false);
                var mutexTest = new ManualResetEvent(false);

                string inputFolder = "";

                //default classifier values
                Options options = null;

                Parser.Default.ParseArguments<Options>(args)
                       .WithParsed(o =>
                       {
                           if (!Directory.Exists(o.InputPath))
                           {
                               Console.WriteLine("ERROR(S):");
                               Console.WriteLine("\tDeclared input path does not exists!");
                               Environment.Exit(-2);
                               return;
                           }
                           else
                           {
                               inputFolder = o.InputPath;
                           }

                           var loggerConfig = new LoggerConfiguration()
                               .WriteTo.Console(outputTemplate: "{Message:lj}{NewLine}{Exception}", restrictedToMinimumLevel: LogEventLevel.Information)
                               .MinimumLevel.Is(o.LogLevel);

                           if (!string.IsNullOrEmpty(o.LogFile))
                           {
                               loggerConfig.WriteTo.File(o.LogFile, outputTemplate: "[{Timestamp:HH:mm:ss.fff}] {Message:lj}{NewLine}{Exception}");
                           }

                           Log = loggerConfig.CreateLogger();

                           options = o;
                       })
                       .WithNotParsed(errors =>
                       {
                           Environment.Exit(-1);
                       });

                Log.Information("Splitting images in {K} folds...", options.KFolds);

                var folds = KFold(inputFolder, folds: options.KFolds);
                var sampleFold = folds.FirstOrDefault().Value;
                var trainingImagesCount = sampleFold.Item1.Count();
                var testImagesCount = sampleFold.Item2.Count();

                Log.Information("Cross-validation training images count: {TrainingImages}", trainingImagesCount);
                Log.Information("Cross-validation test images count: {TestImages}", testImagesCount);

                Log.Information("Processors count: {ProcCount}", Environment.ProcessorCount);
                ThreadPool.GetAvailableThreads(out int availableWorkerThreads, out int availableCompletionPortThreads);
                Log.Information("Thread pool size: {AvailableWorkers} (I/O ports: {CompletionPortsThreads})", availableWorkerThreads, availableCompletionPortThreads);

                Log.Information("Parallelization degree: {Parallelization}", options.Parallelism.ToString());

                //initialize detector
                var detector = new WoodDefectDetector()
                {
                    ShouldReadPreviousResults = false,
                    ShouldSaveResults = options.SaveTrainData,
                    DegreeOfParallelism = options.Parallelism
                };

                //keep track of all times
                var trainExecTime = new List<long>();
                var testExecTime = new List<long>();

                //prepare to save errors count
                var errorRates = new List<double>(options.KFolds);

                //keep track of the progress
                var currentTrainingProgress = 0;
                var currentTestProgress = 0;

                //attach handlers
                detector.TrainingProgress += (sender, e) => {
                    currentTrainingProgress++;
                    var completionPercentage = (double)currentTrainingProgress / trainingImagesCount * 100d;

                    WriteProgress(string.Format("Training progress: {0:##0.##}% ({1}/{2}, {3}ms)", completionPercentage, currentTrainingProgress, trainingImagesCount, e.ExecutionTime));

                    if (completionPercentage == 100d)
                    {
                        Console.WriteLine();
                    }
                };
                detector.TrainingCompleted += (sender, e) =>
                {
                    trainExecTime.Add(e.TotalExecutionTime);
                    currentTrainingProgress = 0;
                    Log.Information("Training completed in: {ExecutionTime:0.###}s", e.TotalExecutionTime / 1000d);
                    mutexTrain.Set();
                };
                detector.TestProgress += (sender, e) => {
                    currentTestProgress++;
                    var completionPercentage = (double)currentTestProgress / testImagesCount * 100d;

                    WriteProgress(string.Format("Testing progress: {0:##0.##}% ({1}/{2}, {3}ms)", completionPercentage, currentTestProgress, testImagesCount, e.ExecutionTime));

                    if (completionPercentage == 100d)
                    {
                        Console.WriteLine();
                    }
                };
                detector.TestCompleted += (sender, e) =>
                {
                    testExecTime.Add(e.TotalExecutionTime);
                    currentTestProgress = 0;
                    Log.Information("Testing completed in: {ExecutionTime:0.###}s", e.TotalExecutionTime / 1000d);
                    mutexTest.Set();
                };

                foreach (var fold in folds)
                {
                    var index = fold.Key;

                    //clear detector training data and reload classifier
                    detector.Reset();                   
                    ForceGarbageCollection();

                    //update results tag
                    detector.ResultTag = string.Format("{0}_{1}", options.TrainDataTag, index);

                    Console.WriteLine("##############################################");

                    Log.Information("Running pass no. {Pass} of cross-validation...", index);
    
                    Log.Information("Loading training images...");

                    var trainingData = new List<Tuple<Image<Gray, byte>, int>>(fold.Value.Item1.Count());
                    var trainingSet = fold.Value.Item1;
                    foreach (var image in trainingSet)
                    {
                        var defectClass = ImageSetUtils.GetDefectClassFromFileName(image);
                        trainingData.Add(new Tuple<Image<Gray, byte>, int>(LoadImage(image), defectClass));
                    }

                    //##########################################################################################

                    Log.Information("Training started...");

                    var notCached = detector.Train(trainingData.Select(i => i.Item1).ToArray(), trainingData.Select(i => i.Item2).ToArray());

                    mutexTrain.WaitOne();

                    //##########################################################################################

                    Log.Information("Unloading training images...");

                    trainingData.Clear();
                    trainingData.ForEach(i => i.Item1.Dispose());
                    trainingSet.Clear();
                    ForceGarbageCollection();

                    //##########################################################################################

                    Log.Information("Loading test images...");

                    var testData = new List<Tuple<Image<Gray, byte>, int>>(fold.Value.Item2.Count());
                    var testSet = fold.Value.Item2;
                    foreach (var image in testSet)
                    {
                        var defectClass = ImageSetUtils.GetDefectClassFromFileName(image);
                        testData.Add(new Tuple<Image<Gray, byte>, int>(LoadImage(image), defectClass));
                    }

                    //##########################################################################################

                    Log.Information("Testing started...");

                    var predictions = new Dictionary<string, Tuple<int, int>>(testData.Count());
                    var testResults = detector.Test(testData.Select(i => i.Item1).ToArray());

                    mutexTest.WaitOne();

                    //##########################################################################################

                    Log.Information("Analyzing results...");

                    for (int i = 0; i < testData.Count(); i++)
                    {
                        var image = testData[i];
                        var fileName = Path.GetFileNameWithoutExtension(testSet.ElementAt(i));
                        var defectClass = image.Item2;
                        var testResult = testResults.ElementAt(i);

                        if (defectClass != testResult.Prediction)
                        {
                            predictions.Add(fileName, new Tuple<int, int>(defectClass, testResult.Prediction));
                        }
                    }

                    var errors = new Dictionary<int, int>();
                    foreach (var entry in predictions)
                    {
                        var filename = entry.Key;
                        var defectClass = entry.Value.Item1;
                        var prediction = entry.Value.Item2;

                        Log.Debug("Prediction of selection {File}: {Prediction} (should be {Class})", filename, prediction, defectClass);

                        if (errors.TryGetValue(defectClass, out var value))
                        {
                            errors[defectClass] = value + 1;
                        }
                        else
                        {
                            errors.Add(defectClass, 1);
                        }
                    }

                    if (errors.Count() > 0)
                    {
                        Log.Information("Error statistics per class:");
                        var errorMsg = new StringBuilder();
                        foreach (var entry in errors)
                        {
                            errorMsg.AppendLine(string.Format("\tClass #{0}: {1}", entry.Key, entry.Value));
                        }
                        Log.Information(errorMsg.ToString().TrimEnd(Environment.NewLine.ToCharArray()));
                    }

                    var testCount = testData.Count();
                    int wrongPredictions = errors.Select(i => i.Value).Sum();

                    Log.Information("Images analyzed: {Count}", testCount);
                    Log.Information("Wrong predictions: {Wrong}", wrongPredictions);

                    double errorRate = (double)wrongPredictions / testCount * 100d;
                    Log.Information("Error rate: {ErrorRate:0.##}%", errorRate);

                    errorRates.Add(errorRate);

                    //##########################################################################################

                    Log.Information("Unloading test images...");

                    testData.Clear();
                    testData.ForEach(i => i.Item1.Dispose());
                    testSet.Clear();
                    ForceGarbageCollection();

                    //##########################################################################################

                    Log.Information("Clearing additional data...");

                    long avgTrain = (long)trainExecTime.Average();
                    trainExecTime.Clear();
                    trainExecTime.Add(avgTrain);

                    long avgTest = (long)testExecTime.Average();
                    testExecTime.Clear();
                    testExecTime.Add(avgTest);

                    errors.Clear();
                    testResults.ToList().ForEach(i => i.Dispose());
                    testResults = null;
                    predictions.Clear();

                    if (options.WaitUserCommand)
                    {
                        Console.Write("Press any key to start the next iteration...");
                        Console.ReadKey();
                    } else
                    {
                        Thread.Sleep(1000);
                    }
                }
                
                Console.WriteLine("##############################################");

                Log.Information("Average training execution time: {AvgExecTime:0.###}s", trainExecTime.Average() / 1000d);
                Log.Information("Average testing execution time: {AvgExecTime:0.###}s", testExecTime.Average() / 1000d);
                Log.Information("Average error rate: {AvgErrorRate:0.##}%", errorRates.Average());
#if DEBUG
                Console.ReadKey();
#endif
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error detected");
            }
        }

        private static void ForceGarbageCollection()
        {
            GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// <summary>
        /// Writes a message in the console in the same line.
        /// </summary>
        /// <param name="message">The message to write</param>
        private static void WriteProgress(string message)
        {
            lock (consoleMutex)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(string.Format("\r{0}", message).PadRight(Console.BufferWidth));
                Console.Out.Flush();
                Console.ResetColor();
            }
        }

#region Helpers
        /// <summary>
        /// Splits the data in the given directory in the specified fold number, 
        /// then returns a mapping between each train set with the respective test set
        /// </summary>
        /// <param name="datasetFolder">The dataset folder</param>
        /// <param name="folds">The number of folds to generate</param>
        /// <returns>A list of tuple with train and test files for each pass</returns>
        private static Dictionary<int, Tuple<List<string>, List<string>>> KFold(string datasetFolder, int folds = 4)
        {

            var files = FileUtils.GetFilesByExtensions(new DirectoryInfo(datasetFolder), ".bmp", ".png", ".jpg");
            var shuffled = files.Shuffle();
            var splittedDataset = shuffled.Split(folds);

            // index of key: test index, value <trainingSet, testSet>
            var trainingTestDictionary = new Dictionary<int, Tuple<List<string>, List<string>>>();
            var count = folds - 1;

            foreach (var trainCombination in splittedDataset.DifferentCombinations(folds - 1))
            {
                var testSet = splittedDataset.ElementAt(count).Select(i => i.FullName).ToList();
                var trainSet = new List<string>();

                foreach (var c in trainCombination)
                {
                    trainSet.AddRange(c.Select(i => i.FullName).ToList());
                }

                var t = new Tuple<List<string>, List<string>>(trainSet, testSet);
                trainingTestDictionary.Add(Math.Abs(count - folds), t);
                count--;
            }
            return trainingTestDictionary;
        }

        /// <summary>
        /// Loads an image into memory.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns>The image</returns>
        private static Image<Gray, byte> LoadImage(string path) {
            return new Image<Bgr, byte>(path).Convert<Gray, byte>();
        }
#endregion
    }
}
